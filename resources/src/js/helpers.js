$(document).ready(function () {
    $('.date').mask('00/00/0000');
    $('.time').mask('00:00:00');
    $('.date_time').mask('00/00/0000 00:00:00');
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.phone').mask('(00) 00000-0009');
    $('.cep').mask('00000-000');
    $('.money').mask('#.##0,00', {reverse: true});
    $('.number').mask('#0');
});


/**
 * Check if the variable is empty
 *
 * @param {*} variable
 * @returns {boolean}
 */
function empty(variable) {
    if(typeof variable == "object")
        return !(Object.keys(variable).length) ? true : false;
    else
        return (variable == undefined || variable == null || variable.length <= 0) ? true : false;
}

/**
 * Returns the base URL with the current segment and, if passed, append another segment path.
 * To rewrite the current segment path, set the second parameter to true
 *
 * @param {string} [segment_path]
 * @param {boolean} [replace]
 * @returns {string}
 */
function url(segment_path, replace) {
    segment_path = empty(segment_path) ? '' : segment_path;
    replace = empty(replace) ? false : replace;

    var url = window.location.href,
        last_separator = n = url.lastIndexOf("/");

    if(replace || ++(last_separator) == url.length)
        url = url.substring(0,n);

    url += '/';
    url += segment_path;
    return url;
}

/**
 * Return the base URL with, if passed, the segment path
 *
 * @param {string} [segment_path]
 * @returns {string}
 */
function base_url(segment_path) {
    segment_path = empty(segment_path) ? '' : segment_path;
    var url  = document.location.origin;

    url += '/';
    url += segment_path;
    return url;
}

/**
 * Redirect the browser to the url passed
 *
 * @param {string} url
 * @returns {undefined}
 */
function redirect(url) {
    window.location.href = url;
}

/**
 *
 * @param obj1
 * @param obj2
 * @returns {{}}
 */
function diff_JSON(obj1, obj2) {
    var ret = {};
    for(var i in obj2) {
        if(!obj1.hasOwnProperty(i) || obj2[i] !== obj1[i]) {
            ret[i] = obj2[i];
        }
    }
    return ret;
}

/**
 *
 * @param value
 * @param key
 * @param myArray
 * @returns {*}
 */
function search_array(value, key, myArray){
    var o = false;
    $.each(myArray, function (i, item) {
           if(eval("item."+key) == value)
               o = item;
    });

    return o;
}

/**
 *
 * @param nStr
 * @returns {string}
 */
function format_number(val, type) {
    type = empty(type) ? 'BRL' : type;

    if(type == 'BRL') {
        val += '';
        var x = val.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    else if(type == 'USD') {
        val = val.replace('.', '');
        val = val.replace(',', '.');
        return val;
    }
}

/**
 *
 */
function print_landscape() {
    var css = '@page { size: landscape; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet){
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);

    window.print();
}

/**
 * Prototype of global object String for capitalizing the string
 *
 * @example
 *      "hello world".capitalize(); => "Hello world"
 *
 * @memberof String
 * @type {object}
 * @namespace String.capitalize
 */
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};


$.fn.getEvents = function() {
    var events = $._data(this.get(0), 'events') || {};
    var response = [];

    if(empty(events))
        return response;

    $.each(events, function (i, value) {
        $.each(value, function (e, event) {
            response.push({'guid' : event.guid, 'event' : event.type, 'namespace' : event.namespace});
        });
    });

    return response;
};

$.fn.hasEvent = function(name,namespace) {
    name = (name !== 'undefined' ? name : null);
    namespace = (namespace !== 'undefined' ? namespace : null);
    var events = $._data(this.get(0), 'events') || {};
    var response = [];

    if(empty(events))
        return response;

    $.each(events,function (i, value) {
        $.each(value, function (e, event) {
            if(event.type == name || event.namespace == namespace)
                response.push({'guid' : event.guid, 'event' : event.type, 'namespace' : event.namespace});
        });
    });

    return response;
};

$.fn.bindFirst = function(event, seletor, fn) {
    // bind as you normally would
    // don't want to miss out on any jQuery magic
    this.on(event, seletor, fn);

    // Thanks to a comment by @Martin, adding support for
    // namespaced events too.
    this.each(function() {
        var handlers = $._data(this, 'events')[event.split('.')[0]];
        // take out the handler we just inserted from the end
        var handler = handlers.pop();
        // move it at the beginning
        handlers.splice(0, 0, handler);
    });
};