$(function() {
    // dashboard init functions
    altair_dashboard.init();
});

altair_dashboard = {
    init : function () {
        'use strict';

        // calendar
        altair_dashboard.clndr_calendar();

        $window.load(function(){
            altair_dashboard.count_animated();
        });
    },

    // animated numerical values
    count_animated: function () {
        $('.countUpMe').each(function () {
            var target = this,
                countTo = $(target).text();
            theAnimation = new CountUp(target, 0, countTo, 0, 2);
            theAnimation.start();
        });
    },

    // calendar
    clndr_calendar: function () {

        var $clndr_events = $('#clndr_events');

        if ($clndr_events.length) {

            var calendar_template = $('#clndr_events_template'),
                template = calendar_template.html(),
                template_compiled = Handlebars.compile(template);

            var daysOfTheWeek = [];

            for (var i = 0; i < 7; i++) {
                daysOfTheWeek.push(moment().weekday(i).format('dd'));
            }
            theCalendar = $clndr_events.clndr({
                weekOffset: 1, // Monday
                daysOfTheWeek: daysOfTheWeek,
                events: clndrEvents,
                render: function(data) {
                    return template_compiled(data);
                },
                clickEvents: {
                    click: function (target) {
                        if(target.events.length) {

                            var $clndr_events_panel = $('.clndr_events'),
                                thisDate = target.date._i;

                            $(target.element)
                                .siblings('.day').removeClass('day-active')
                                .end()
                                .addClass('day-active');

                            if($clndr_events_panel.children('[data-clndr-event=' + thisDate + ']').length) {

                                $clndr_events_panel
                                    .children('.clndr_event')
                                    .hide();

                                if (!$clndr_events.hasClass('events_visible')) {
                                    // adjust events panel
                                    dayWidthCheck();
                                    $clndr_events.addClass('events_visible');
                                    $clndr_events_panel
                                        .children('[data-clndr-event=' + thisDate + ']').velocity("transition.slideUpIn", {
                                        stagger: 100,
                                        drag: true,
                                        delay: 280
                                    });
                                } else {
                                    $clndr_events_panel
                                        .children('[data-clndr-event=' + thisDate + ']').velocity("transition.slideUpIn", {
                                        stagger: 100,
                                        drag: true
                                    });
                                }
                            } else if( $(target.element).hasClass('last-month') ) {
                                setTimeout(function() {
                                    $clndr_events.find('.calendar-day-' + target.date._i).click()
                                },380);
                                $clndr_events.find('.clndr_previous').click();
                            } else if( $(target.element).hasClass('next-month') ) {
                                setTimeout(function() {
                                    $clndr_events.find('.calendar-day-' + target.date._i).click()
                                },380);
                                $clndr_events.find('.clndr_next').click();
                            }
                        }
                    }
                }
            });

            var animate_change = function() {
                $clndr_events
                    .addClass('animated_change')
                    .removeClass('events_visible');
                setTimeout(function() {
                    $clndr_events
                        .removeClass('animated_change')
                    ;
                },380);
            };

            var refresh_events = function () {
                $('.clndr_days_grid .event').each(function (i, event_now) {
                    var date = $(event_now).data('date');
                    var has_date = search_array(date, 'date', theCalendar.options.events);

                    if(!has_date)
                        $(event_now).removeClass('event');
                });
            }

            var remove_event = function (event_id) {
                var index = null;
                $.each(theCalendar.options.events, function (i, event) {
                    if(event.id == event_id)
                        index = i;
                });
                theCalendar.options.events.splice(index, 1);
                $clndr_events.removeClass('events_visible events_over');
                refresh_events();
            };
            

            // next month
            $clndr_events.on('click', '.clndr_next', function(e) {
                e.preventDefault();

                animate_change();
                setTimeout(function() {
                    theCalendar.forward();
                },280);
            });

            // previous month
            $clndr_events.on('click', '.clndr_previous', function(e) {
                e.preventDefault();

                animate_change();
                setTimeout(function() {
                    theCalendar.back();
                },280);
            });

            // today
            $clndr_events.on('click', '.clndr_today', function(e) {
                e.preventDefault();

                animate_change();
                setTimeout(function() {
                    theCalendar
                        .setYear(moment().format('YYYY'))
                        .setMonth(moment().format('M') - 1);
                },280);

            });

            // close events
            $clndr_events.on('click', '.clndr_events_close_button', function () {
                $clndr_events.removeClass('events_visible events_over')
            });

            // add event modal
            event_modal = UIkit.modal("#modal_clndr_new_event");
            $clndr_events.on('click', '.clndr_add_event', function () {
                if ( event_modal.isActive() ) {
                    event_modal.hide();
                } else {
                    event_modal.show();
                    // hide events panel
                    $clndr_events.removeClass('events_visible');
                    setTimeout(function() {
                        $window.resize();
                    },280)
                }
            });

            // add events submit
            $('#clndr_new_event_submit').on('click', function() {

                var e_title = '#clndr_event_title_control',
                    e_link = '#clndr_event_link_control',
                    e_date = '#clndr_event_date_control',
                    e_start = '#clndr_event_start_control',
                    e_end = '#clndr_event_end_control';

                if($(e_title).val() == '') {
                    $(e_title).addClass('md-input-danger').focus();
                    altair_md.update_input($(e_title));
                    return false;
                }

                if($(e_date).val() == '') {
                    $(e_date).addClass('md-input-danger').focus();
                    altair_md.update_input($(e_date));
                    return false;
                }

                var new_event = [
                    { date: $(e_date).val(), title: $(e_title).val(), url: $(e_link).val() ? $(e_link).val() : 'javascript:void(0)', timeStart: $(e_start).val(), timeEnd: $(e_end).val() }
                ];

                $.ajax({
                    url: base_url('service/add_event'),
                    type: 'POST',
                    data: new_event[0],
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if(data.status == 1) {
                            new_event[0].id = data.event_id;
                            theCalendar.addEvents(new_event);
                        }
                        else {
                            UIkit.notify({
                                message : data.message,
                                status  : 'danger',
                                timeout : 5000,
                                pos     : 'bottom-center'
                            });
                        }
                    },
                    error: function () {
                        UIkit.notify({
                            message : 'Connection fail',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });

                theCalendar.setMonth(moment($(e_date).val()).format('M') - 1);
                // hide modal
                event_modal.hide();

                $(e_title+','+e_link+','+e_date+','+e_start+','+e_end).removeClass('md-input-danger').val('');
                altair_md.update_input($(e_title+','+e_link+','+e_date+','+e_start+','+e_end));

            });

            // delete events
            $(document).on('click','.delete-event', function () {
                var event = $(this).parents('.clndr_event');
                var event_id = event.data('id');

                $.ajax({
                    url: base_url('service/delete_event'),
                    type: 'POST',
                    data: {'event_id' : event_id},
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if(data.status == 1) {
                            event.remove();
                            remove_event(event_id);
                        }
                        else {
                            UIkit.notify({
                                message : data.message,
                                status  : 'danger',
                                timeout : 5000,
                                pos     : 'bottom-center'
                            });
                        }
                    },
                    error: function () {
                        UIkit.notify({
                            message : 'Connection fail',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });

            });

            var dayWidth = $clndr_events.find('.day > span').outerWidth(),
                calMinWidth = dayWidth * 7 + 240 + 32 + 14; // day + events container + padding-left/padding-right + day padding (7*2px)

            function dayWidthCheck() {
                ($clndr_events.width() < (calMinWidth)) ? $clndr_events.addClass('events_over') : $clndr_events.removeClass('events_over');
            }

            dayWidthCheck();

            // resize map on window resize event
            $(window).on('debouncedresize', function () {
                dayWidthCheck();
            })
        }
    },
};