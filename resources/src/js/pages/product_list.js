$(function() {
    // product list
    product_list.init();

    // category list
    category_list.init();

    // unit type list
    unit_list.init();

    // search product
    search_product.init();
});

$("div#wrapper_fields_category").bindFirst('click.first', '.btnSectionRemove', function (e) {
    e.preventDefault();
    var $this = $(this);
    var categoria = $this.parents('.form_section').find('input.md-input').val();

    UIkit.modal.confirm('Tem certeza que deseja remover a categoria <span class="md-color-grey-900">'+categoria+'</span>', function(){$this.parents('.form_section').remove()});

    e.stopImmediatePropagation();
});

search_product = {
    init : function () {
        //search
        $("#submit_search").on('click',function () {
            var filter_in_stock = !empty($("#filter_in_stock:checked").val()) ? true : "";
            window.search_keyword = $("#search_name").val();
            window.search_filter = {'stock' : filter_in_stock, 'category' : $("#category_list").val()};

            var status = ( !empty(window.search_keyword) || !empty(window.search_filter.stock) || !empty(window.search_filter.category) ) ? true : false;
            if(status) {
                altair_helpers.content_preloader_show('md');
                $.ajax({
                    url: base_url('product/search'),
                    type: 'POST',
                    data: {'search' : window.search_keyword, 'filter' : window.search_filter},
                    dataType: 'json',
                    success: function (data) {
                        if(data.status) {
                            search_product.clear();

                            $.each(data.results, function (key, value) {
                                search_product.add(value);
                            });

                            search_product.pagination(data.pagination);

                            search_product.show();
                        }
                        else
                            search_product.show_error();

                        altair_helpers.content_preloader_hide();
                    },
                    error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });
            }
        });

        // clear result
        $("#search_name").on('focusout', function () {
            var value = $(this).val();

            if(empty(value))
                search_product.hide();
        });

        // next result
        $(document).on('click', '#result_list .uk-pagination li a', function () {
            var cur_page = $(this).data('ci-pagination-page');

            altair_helpers.content_preloader_show('md');
            $.ajax({
                    url: base_url('product/search'),
                   type: 'POST',
                   data: {'search': window.search_keyword, 'cur_page': cur_page},
               dataType: 'json',
                success: function (data) {
                        altair_helpers.content_preloader_hide();

                        if(data.status) {
                            search_product.clear();

                            console.log(data);

                            $.each(data.results, function (key, value) {
                                search_product.add(value);
                            });

                            search_product.pagination(data.pagination);
                        }
                        else {
                            search_product.show_error();
                        }

                    },
                  error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
            });

            return false;
        });

        // back search
        $(document).on('click', '#back_search', function () {
            $("#search_name").val('');
            search_product.hide();
        });
    },
    show : function () {
        $("#product_list").hide();
        $("#result_error").hide();
        $("#result_list").show();
    },
    show_error: function () {
        $("#product_list").hide();
        $("#result_list").hide();
        $("#result_error").show();
    },
    hide : function () {
        $("#result_list").hide();
        $("#result_error").hide();
        $("#product_list").show();
        this.clear();
    },
    add : function (data) {
        var qnt = empty(data.qnt) ? '0' : data.qnt;
        var category = product_list.get_category(data.category);
        category = !empty(category) ? category.name : '';
        var $row = '<tr data-id="'+data.id+'">' +
            '<td class="product_description">'+data.description+'</td>' +
            '<td class="product_categoria" data-id="'+data.category+'">'+category+'</td>' +
            '<td>'+qnt+'</td>' +
            '<td>' +
                '<a class="view" href="'+base_url('product/view/'+data.id)+'"><i class="md-icon material-icons">visibility</i></a>' +
                '<a class="edit" href="'+base_url('product/view/'+data.id+'/edit')+'"><i class="md-icon material-icons">create</i></a>' +
                '<a class="delete" href="#"><i class="md-icon material-icons">delete</i></a>' +
            '</td>' +
            '</tr>';
        $("#result_list table tbody").append($row);
    },
    pagination: function (data) {
        $("#result_list > div").append(data);
    },
    clear: function () {
        $("#result_list table tbody").html('');
        $("#result_list > div").html('');
    }
}

product_list = {
    init : function () {
        $("div#product_list table.uk-table tbody tr").each(function (i, element) {
            var $target = $(element).children('.product_category');
            var category = product_list.get_category($target.data('id'));

            if(category)
                $target.text(category.name);
        });

        $(document).on('click', '#product_list a.delete', function () {
            var product_id = $(this).parents('tr').data("id");
            var product_name = $.trim($(this).parents('tr').children('td.product_description').text());

            UIkit.modal.confirm('Tem certeza que deseja remover o produto '+product_name+'?', function(){

                altair_helpers.content_preloader_show('md');
                $.ajax({
                        url: base_url('product/delete'),
                       type: 'POST',
                       data: {'product_id' : product_id},
                   dataType: 'json',
                    success: function (data) {
                            altair_helpers.content_preloader_hide();

                            if(data.status == 1)
                                $('tr[data-id='+product_id+']').remove();
                            else {
                                UIkit.notify({
                                    message : data.message,
                                    status  : 'danger',
                                    timeout : 5000,
                                    pos     : 'bottom-center'
                                });
                            }
                        },
                      error: function () {
                            altair_helpers.content_preloader_hide();

                            UIkit.notify({
                                message : 'Connection fail',
                                status  : 'danger',
                                timeout : 5000,
                                pos     : 'bottom-center'
                            });
                        }
                });
            });

            return false;
        });
    },
    get_category : function (id) {
        var index = null;

        $.each(window._category_list, function (i, item) {
            if(item.id == id) {
                index = item;
                return;
            }
        });

        return index;
    },
    update_category : function (id, name) {
        $("div#product_list table.uk-table tbody tr td.product_category[data-id="+id+"]").each(function (i, element) {
            $(element).text(name);
        });
    },
    remove_category : function (id) {
        $("div#product_list table.uk-table tbody tr td.product_category[data-id="+id+"]").each(function (i, element) {
            $(element).text('');
        });
    }
};

category_list = {
    select : new Selectize("category_list").init(),
    init : function () {
        this.select;

        this.disabled();

        $("#close_category").on('click', function () {
            category_list.disabled();
        });

        $("#edit_category").on('click', function () {
            category_list.enabled();
        });

        $("#save_category").on('click', function () {
            var category = new Array();
            category.origin = window._category_list;
            category.now    = category_list.get();
            category.change = {'add': [], 'del': [], 'update':[]};

            $.each(category.now, function (i, category_now) {
                var category_origin = search_array(category_now.id, 'id', category.origin);
                if(category_origin) {
                    var changes = diff_JSON(category_origin, category_now);
                    if(!empty(changes))
                        category.change.update.push(category_now);
                }
                else
                    category.change.add.push(category_now);
            });
            $.each(category.origin, function (i, category_origin) {
                var category_del = search_array(category_origin.id, 'id', category.now);

                if(!category_del)
                    category.change.del.push(category_origin);
            });

            var status = ( !empty(category.change.add) || !empty(category.change.del) || !empty(category.change.update) ) ? true : false;

            if(status) {
                altair_helpers.content_preloader_show('md');

                $.ajax({
                     url: base_url('product/category'),
                    type: 'POST',
                    data: {'data' : category.change},
                dataType: 'json',
                 success: function (data) {
                         altair_helpers.content_preloader_hide();

                        if(data.status == 1) {
                            UIkit.modal("#modal_category").hide();
                            category_list.disabled();

                            if(!empty(data.response)) {
                                $.each(data.response, function (i, category) {
                                    var label = "category__" + category.id;
                                    var $category = $("#wrapper_fields_category .form_section [data-id="+category.target+"]");
                                    $category.data('id', category.id);
                                    $category.find('input.md-input').data('id', category.id).prop('id', label).prop('name', label);
                                    $category.find('.uk-input-group label').prop('for', label);
                                    $("#wrapper_fields_category").prop('dynamic-fields-counter', ++category.id);

                                    category_list.add(category.id, category.name);
                                });
                                category_list.select.refresh();
                            }

                            if(!empty(category.change.update)) {
                                $.each(category.change.update, function (i, category) {
                                    category_list.update(category.id, category.name);
                                    product_list.update_category(category.id, category.name);
                                });
                            }

                            if(!empty(category.change.del)) {
                                $.each(category.change.del, function (i, category) {
                                    category_list.remove(category.id);
                                    product_list.remove_category(category.id);
                                });
                            }
                        }
                        else {
                            $.each(data.message, function (i, message) {
                                UIkit.notify({
                                    message : message.error,
                                    status  : 'danger',
                                    timeout : 5000,
                                    pos     : 'bottom-center'
                                });
                            });
                        }
                    },
                    error: function () {
                         altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });
            }
            else
                category_list.disabled();
        });
    },
    get : function () {
        var o = [];

        $("#wrapper_fields_category .form_section").each(function (i, element) {
            var category_id = $(element).find('input.md-input').data('id');
            var category_name = $(element).find('input.md-input').val();

            if(!empty(category_name))
                o.push({'id' : String(category_id), 'name' : category_name})
        });

        return o;
    },
    add : function (id, name) {
        this.select.add(id,name);

        window._category_list.push({'id' : String(id), 'name' : name});
    },
    update : function (id, name) {
        this.select.update(id, name);

        $.each(window._category_list, function (i, item) {
            if(item.id == id) {
                window._category_list[i].name = name;
                return;
            }
        });
    },
    remove : function (id) {
        this.select.remove(id);

        var index = false;
        $.each(window._category_list, function (i, item) {
            if(item.id == id) {
                index = i;
                return;
            }
        });
        if(index)
            window._category_list.splice(index,1);
    },
    enabled : function () {
        $("#wrapper_fields_category input").prop('disabled', false);
        $("#wrapper_fields_category a.btnSectionRemove").prop('disabled', false);
        $("#wrapper_fields_category a.btnSectionClone").prop('disabled', false);
        $("#modal_category #save_category").show();
        $("#modal_category #edit_category").hide();
    },
    disabled : function () {
        $("#wrapper_fields_category input").prop('disabled', true);
        $("#wrapper_fields_category a.btnSectionRemove").prop('disabled', true);
        $("#wrapper_fields_category a.btnSectionClone").prop('disabled', true);
        $("#modal_category #save_category").hide();
        $("#modal_category #edit_category").show();
    },
};

unit_list = {
    init : function () {
        this.disabled();

        $("#close_unit").on('click', function () {
            unit_list.disabled();
        });

        $("#edit_unit").on('click', function () {
            unit_list.enabled();
        });

        $("#save_unit").on('click', function () {
            var unit = new Array();
            unit.origin = window._unit_list;
            unit.now    = unit_list.get();
            unit.change = {'add': [], 'del': [], 'update':[]};

            $.each(unit.now, function (i, unit_now) {
                var unit_origin = search_array(unit_now.id, 'id', unit.origin);
                if(unit_origin) {
                    var changes = diff_JSON(unit_origin, unit_now);
                    if(!empty(changes))
                        unit.change.update.push(unit_now);
                }
                else
                    unit.change.add.push(unit_now);
            });
            $.each(unit.origin, function (i, unit_origin) {
                var unit_del = search_array(unit_origin.id, 'id', unit.now);

                if(!unit_del)
                    unit.change.del.push(unit_origin);
            });

            var status = ( !empty(unit.change.add) || !empty(unit.change.del) || !empty(unit.change.update) ) ? true : false;

            if(status) {
                altair_helpers.content_preloader_show('md');

                $.ajax({
                    url: base_url('product/unit'),
                    type: 'POST',
                    data: {'data' : unit.change},
                    dataType: 'json',
                    success: function (data) {
                        altair_helpers.content_preloader_hide();

                        if(data.status == 1) {
                            UIkit.modal("#modal_unit_type").hide();
                            unit_list.disabled();

                            if(!empty(data.response)) {
                                $.each(data.response, function (i, unit) {
                                    var label = "unit__" + unit.id;
                                    var $unit = $("#wrapper_fields_unit_type .form_section [data-id="+unit.target+"]");
                                    $unit.data('id', unit.id);
                                    $unit.find('input.md-input').data('id', unit.id).prop('id', label).prop('name', label);
                                    $unit.find('.uk-input-group label').prop('for', label);
                                    $("#wrapper_fields_unit_type").prop('dynamic-fields-counter', ++unit.id);

                                    unit_list.add(unit.id, unit.name);
                                });
                            }

                            if(!empty(unit.change.update)) {
                                $.each(unit.change.update, function (i, unit) {
                                    unit_list.update(unit.id, unit.name);
                                });
                            }

                            if(!empty(unit.change.del)) {
                                $.each(unit.change.del, function (i, unit) {
                                    unit_list.remove(unit.id);
                                });
                            }
                        }
                        else {
                            $.each(data.message, function (i, message) {
                                UIkit.notify({
                                    message : message.error,
                                    status  : 'danger',
                                    timeout : 5000,
                                    pos     : 'bottom-center'
                                });
                            });
                        }
                    },
                    error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });
            }
            else
                unit_list.disabled();
        });
    },
    get : function () {
        var o = [];

        $("#wrapper_fields_unit_type .form_section").each(function (i, element) {
            var unit_id = $(element).find('input.md-input').data('id');
            var unit_name = $(element).find('input.md-input').val();

            if(!empty(unit_name))
                o.push({'id' : String(unit_id), 'name' : unit_name})
        });

        return o;
    },
    add : function (id, name) {
        window._unit_list.push({'id' : String(id), 'name' : name});
    },
    update : function (id, name) {
        $.each(window._unit_list, function (i, item) {
            if(item.id == id) {
                window._unit_list[i].name = name;
                return;
            }
        });
    },
    remove : function (id) {
        var index = false;

        $.each(window._unit_list, function (i, item) {
            if(item.id == id) {
                index = i;
                return;
            }
        });

        if(index)
            window._unit_list.splice(index,1);
    },
    enabled : function () {
        $("#wrapper_fields_unit_type input").prop('disabled', false);
        $("#wrapper_fields_unit_type a.btnSectionRemove").prop('disabled', false);
        $("#wrapper_fields_unit_type a.btnSectionClone").prop('disabled', false);
        $("#modal_unit_type #save_unit").show();
        $("#modal_unit_type #edit_unit").hide();
    },
    disabled : function () {
        $("#wrapper_fields_unit_type input").prop('disabled', true);
        $("#wrapper_fields_unit_type a.btnSectionRemove").prop('disabled', true);
        $("#wrapper_fields_unit_type a.btnSectionClone").prop('disabled', true);
        $("#modal_unit_type #save_unit").hide();
        $("#modal_unit_type #edit_unit").show();
    },
};

function Selectize(selector) {
    this.selector = selector;

    this.instance = function () {
        var selector = this.selector;
        var $selector = $("#"+selector+"").selectize({
            plugins: {
                'remove_button': {
                    label : ''
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });
        var selectize = $selector[0].selectize;

        return selectize;
    }

    this.init = function () {
        this.instance();

        return this;
    }

    this.enable = function () {
        var $this = this.instance();
        $this.enable();

        return this;
    }

    this.disable = function () {
        var $this = this.instance();
        $this.disable();

        return this;
    }

    this.load_list = function (list) {
        var $this = this.instance();
        $.each(list,function (id,value) {
            $this.addOption({value:value.id,text:value.name});
            $this.addItem(value,true);
        });
        $this.refreshOptions();
        $this.refreshItems();

        return this;
    }

    this.select = function (id) {
        var $this = this.instance();
        $this.setValue(id);

        return this;
    }

    this.add = function (value, text) {
        var $this = this.instance();

        $this.addOption({value:value,text:text});
        $this.refreshOptions();
        $this.addItem(value,true);
        $this.refreshItems();

        return this;
    }

    this.update = function (value, text) {
        var $this = this.instance();

        $this.updateOption(value,{text: text, value: String(value), disabled: false});

        return this;
    }
    
    this.remove = function (id) {
        var $this = this.instance();
        $this.removeOption(id, true);

        return this;
    }

    this.empty = function () {
        this.refresh();
        var $this = this.instance();
        $this.clearOptions();

        return this;
    }

    this.refresh = function () {
        var $this = this.instance();
        $this.clear();

        return this;
    }

    this.options = function () {
        var $this = this.instance();
        return $this.options;
    }
}