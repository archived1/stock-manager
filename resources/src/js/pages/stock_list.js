$(function() {
    search_stock.init();

    stock_list.init();
});

search_stock = {
    select : new Selectize("stock_status").init(),

    init : function () {
        this.select;

        this.date_range();
    },
    date_range: function() {
        var $dp_start = $('#uk_dp_start'),
            $dp_end = $('#uk_dp_end');

        var start_date = UIkit.datepicker($dp_start, {
            format:'DD/MM/YYYY'
        });

        var end_date = UIkit.datepicker($dp_end, {
            format:'DD/MM/YYYY'
        });

        $dp_start.on('change',function() {
            end_date.options.minDate = $dp_start.val();
        });

        $dp_end.on('change',function() {
            start_date.options.maxDate = $dp_end.val();
        });
    },
    clear : function () {
        $('#uk_dp_start').val('').blur();
        $('#uk_dp_end').val('').blur();
        search_stock.select.refresh();
        window.filter = {date_start: null, date_end: null, status: null, type: null};
    }
}

stock_list = {
    init : function () {
        $(document).on('click', 'a.delete', function () {
            var row = $(this).parents('tr');
            var order_id = row.data("id");

            UIkit.modal.confirm('Tem certeza que deseja remover o pedido de nº'+order_id, function(){

                altair_helpers.content_preloader_show('md');
                $.ajax({
                    url: base_url('stock/delete'),
                    type: 'POST',
                    data: {'order_id' : order_id},
                    dataType: 'json',
                    success: function (data) {
                        altair_helpers.content_preloader_hide();

                        if(data.status == 1)
                            $('tr[data-id='+order_id+']').remove();
                        else {
                            UIkit.notify({
                                message : data.message,
                                status  : 'danger',
                                timeout : 5000,
                                pos     : 'bottom-center'
                            });
                        }
                    },
                    error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'Connection fail',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });
            });

            return false;
        });

        $("#submit_search").click(function () {
            filter = {
                    date_start : $("#uk_dp_start").val(),
                    date_end : $("#uk_dp_end").val(),
                    status : $("#stock_status").val(),
                    type: window.order_type
            };

            var status = ( !empty(filter.date_start) || !empty(filter.date_end) || !empty(filter.status) ) ? true : false;
            if(status) {
                altair_helpers.content_preloader_show('md');
                $.ajax({
                    url: base_url('stock/search'),
                    type: 'POST',
                    data: {'filter' : filter},
                    dataType: 'json',
                    success: function (data) {
                        var order_types = {1 : 'entry', 2 : 'out'};
                        var type = order_types[order_type];

                        if(data.status) {
                            stock_list[type].clear_result();
                            $.each(data.results, function (i,item) {
                                stock_list[type].add(item,false);
                            });

                            stock_list[type].pagination(data.pagination,false);
                            stock_list[type].show();
                        }
                        else
                            stock_list[type].show_error();

                        altair_helpers.content_preloader_hide();
                    },
                    error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                });
            }
        });

        // change order type
        $(".uk-tab li a").click(function () {
            window.order_type = $(this).data('type');
        });

        // next result
        $(document).on('click', '#pagination_entry > .uk-pagination > li a, #pagination_out > .uk-pagination > li a', function () {
            var cur_page = $(this).data('ci-pagination-page');
            var order_type = $(this).parents('.wrapper_pagination').data('type');

            altair_helpers.content_preloader_show('md');
            $.ajax({
                     url: base_url('stock/search'),
                    type: 'POST',
                    data: {'cur_page' : cur_page, 'type' : order_type},
                dataType: 'json',
                 success: function (data) {
                     var order_types = {1 : 'entry', 2 : 'out'};
                     var type = order_types[order_type];

                     if(data.status) {
                        stock_list[type].clear();

                        $.each(data.results, function (i, item) {
                           stock_list[type].add(item);
                        });

                        stock_list[type].pagination(data.pagination);
                     }

                    altair_helpers.content_preloader_hide();
                 },
                   error: function () {
                        altair_helpers.content_preloader_hide();

                        UIkit.notify({
                            message : 'An error occurred on the server! :(',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
            });

            return false;
        });

        // next result search
        $(document).on('click', '.result_list > .wrapper_pagination > .uk-pagination > li a', function () {
            var cur_page = $(this).data('ci-pagination-page');
            var order_type = $(this).parents('.wrapper_pagination').data('type');

            altair_helpers.content_preloader_show('md');
            $.ajax({
                url: base_url('stock/search'),
                type: 'POST',
                data: {'filter' : filter, 'cur_page' : cur_page, 'type' : order_type},
                dataType: 'json',
                success: function (data) {
                    var order_types = {1 : 'entry', 2 : 'out'};
                    var type = order_types[order_type];

                    if(data.status) {
                        stock_list[type].clear_result();

                        $.each(data.results, function (i, item) {
                            stock_list[type].add(item,false);
                        });

                        stock_list[type].pagination(data.pagination,false);
                    }

                    altair_helpers.content_preloader_hide();
                },
                error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });

            return false;
        });

        // back search
        $(".result_error a.back_search").click(function () {
           var type = $(this).data('type');

           stock_list[type].hide();
           stock_list[type].clear_result();
           search_stock.clear();
        });
    },
    entry : {
        show : function () {
            $("#stock_entry > table").hide();
            $("#stock_entry > #pagination_entry").hide();
            $("#stock_entry > .result_list").show();
            $("#stock_entry > .result_error").hide();
        },
        show_error : function () {
            $("#stock_entry > table").hide();
            $("#stock_entry > #pagination_entry").hide();
            $("#stock_entry > .result_list").hide();
            $("#stock_entry > .result_error").show();
        },
        hide : function () {
            $("#stock_entry > table").show();
            $("#stock_entry > #pagination_entry").show();
            $("#stock_entry > .result_list").hide();
            $("#stock_entry > .result_error").hide();
        },
        add : function (data, target) {
            target = empty(target) ? true : target;
            target = target ? '#stock_entry > table > tbody' : '#stock_entry > .result_list > table > tbody';

            data.provider = window.provider_list[data.provider].trade;
            data.date = new Date(data.date).toLocaleDateString();
            data.status = window.status_list[data.status];

            var $row = '<tr data-id="'+data.id+'">' +
                            '<td class="stock_code">'+data.id+'</td>' +
                            '<td class="stock_provider">'+data.provider+'</td>' +
                            '<td class="stock_date">'+data.date+'</td>' +
                            '<td class="stock_status">'+data.status+'</td>' +
                            '<td>' +
                                '<a href="'+base_url('stock/entry/'+data.id)+'" class="view"><i class="md-icon material-icons">visibility</i></a>' +
                                '<a href="'+base_url('stock/entry/'+data.id+'/edit')+'" class="edit"><i class="md-icon material-icons">edit</i></a>' +
                                '<a href="#" class="delete"><i class="md-icon material-icons">delete</i></a>' +
                            '</td>' +
                       '</tr>';
            $(target).append($row);
        },
        pagination : function (data, target) {
            target = empty(target) ? true : target;
            target = target ? '#stock_entry > #pagination_entry' : '#stock_entry > .result_list > div';
            $(target).html(data);
        },
        clear : function () {
            $("#stock_entry > table > tbody").html('');
            $("#stock_entry > #pagination_entry").html('');
        },
        clear_result : function () {
            $("#stock_entry > .result_list > table > tbody").html('');
            $("#stock_entry > .result_list > div").html('');
        }
    },
    out : {
        show : function () {
            $("#stock_out > table").hide();
            $("#stock_out > #pagination_out").hide();
            $("#stock_out > .result_list").show();
            $("#stock_out > .result_error").hide();
        },
        show_error : function () {
            $("#stock_out > table").hide();
            $("#stock_out > #pagination_out").hide();
            $("#stock_out > .result_list").hide();
            $("#stock_out > .result_error").show();
        },
        hide : function () {
            $("#stock_out > table").show();
            $("#stock_out > #pagination_out").show();
            $("#stock_out > .result_list").hide();
            $("#stock_out > .result_error").hide();
        },
        add : function (data, target) {
            target = empty(target) ? true : target;
            target = target ? '#stock_out > table > tbody' : '#stock_out > .result_list > table > tbody';

            data.provider = window.provider_list[data.provider].trade;
            data.date = new Date(data.date).toLocaleDateString();
            data.status = window.status_list[data.status];

            var $row = '<tr data-id="'+data.id+'">' +
                '<td class="stock_code">'+data.id+'</td>' +
                '<td class="stock_provider">'+data.provider+'</td>' +
                '<td class="stock_date">'+data.date+'</td>' +
                '<td class="stock_status">'+data.status+'</td>' +
                '<td>' +
                '<a href="'+base_url('stock/entry/'+data.id)+'" class="view"><i class="md-icon material-icons">visibility</i></a>' +
                '<a href="'+base_url('stock/entry/'+data.id+'/edit')+'" class="edit"><i class="md-icon material-icons">edit</i></a>' +
                '<a href="#" class="delete"><i class="md-icon material-icons">delete</i></a>' +
                '</td>' +
                '</tr>';
            $(target).append($row);
        },
        pagination : function (data, target) {
            target = empty(target) ? true : target;
            target = target ? '#stock_out > #pagination_out' : '#stock_out > .result_list > div';
            $(target).html(data);
        },
        clear : function () {
            $("#stock_out > table > tbody").html('');
            $("#stock_out > #pagination_out").html('');
        },
        clear_result : function () {
            $("#stock_out > .result_list > table > tbody").html('');
            $("#stock_out > .result_list > div").html('');
        }
    }
}

function Selectize(selector) {
    this.selector = selector;

    this.instance = function () {
        var selector = this.selector;
        var $selector = $("#"+selector+"").selectize({
            plugins: {
                'remove_button': {
                    label : ''
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });
        var selectize = $selector[0].selectize;

        return selectize;
    }

    this.init = function () {
        this.instance();

        return this;
    }

    this.enable = function () {
        var $this = this.instance();
        $this.enable();

        return this;
    }

    this.disable = function () {
        var $this = this.instance();
        $this.disable();

        return this;
    }

    this.load_list = function (list) {
        var $this = this.instance();
        $.each(list,function (id,value) {
            $this.addOption({value:value.id,text:value.name});
            $this.addItem(value,true);
        });
        $this.refreshOptions();
        $this.refreshItems();

        return this;
    }

    this.select = function (id) {
        var $this = this.instance();
        $this.setValue(id);

        return this;
    }

    this.add = function (value, text) {
        var $this = this.instance();

        $this.addOption({value:value,text:text});
        $this.refreshOptions();
        $this.addItem(value,true);
        $this.refreshItems();

        return this;
    }

    this.update = function (value, text) {
        var $this = this.instance();

        $this.updateOption(value,{text: text, value: String(value), disabled: false});

        return this;
    }

    this.remove = function (id) {
        var $this = this.instance();
        $this.removeOption(id, true);

        return this;
    }

    this.empty = function () {
        this.refresh();
        var $this = this.instance();
        $this.clearOptions();

        return this;
    }

    this.refresh = function () {
        var $this = this.instance();
        $this.clear();

        return this;
    }

    this.options = function () {
        var $this = this.instance();
        return $this.options;
    }
}