$(function() {
    $("form#login").submit(function () {
        altair_helpers.content_preloader_show('md');
        $("form#login button").prop("disabled",true).addClass('disabled');

        $.ajax({
                 url: base_url('login'),
                type: 'POST',
                data: $("form#login").serialize(),
            dataType: 'json',
             success: function (data) {
                    altair_helpers.content_preloader_hide();
                    $("form#login button").prop("disabled",false).removeClass('disabled');

                    if(data.status == 1)
                        redirect(data.url);
                    else {
                        UIkit.notify({
                            message : data.mensagem,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
             },
               error: function () {
                    altair_helpers.content_preloader_hide();
                    $("form#login button").prop("disabled",false).removeClass('disabled');

                    UIkit.notify({
                        message : 'Connection fail',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
               }
        });

        return false;
    });
});