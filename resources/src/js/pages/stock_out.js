$(function () {
    stock_list.init();

    $("#print").click(function () {
        print_landscape();
    });
    $("#edit").click(function () {
        stock_list.enabled();
    });
    $("#save").click(function () {
        save('pending');
    });
    $("#finish").click(function () {
        save('finish');
    });
});

stock_list = {
    init : function () {
        // icheck
        $(".discount_type").iCheck({
            checkboxClass: 'icheckbox_md',
            radioClass: 'iradio_md small',
            increaseArea: '20%'
        });

        this.status();

        // loads calcs
        $("#amount").val(format_number(calc_amount())).blur();
        calc_discount();

        // search product
        $("#product_search_name").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: base_url('stock/search_product'),
                   type: 'POST',
               dataType: "json",
                   data: {'keyword' : request.term },
                success: function(data) {
                        if(data.status) {
                            response($.map(data.results, function (item) {
                                return {
                                    label: item.description,
                                    value: item.description,
                                    id: item.id,
                                    price: item.price,
                                    qnt: item.qnt
                                }
                            }));
                        }
                    }
                });
            },
            minLength: 3,
            select: function( event, ui ) {
                $("#product_search_name")
                                         .data('id', ui.item.id)
                                         .data('description', ui.item.value)
                                         .data('price', ui.item.price)
                                         .data('qnt', ui.item.qnt);
            }
        });

        // add product
        $("#add_product").click(function () {
            var product_id = $("#product_search_name").data('id');
            var product_description = $("#product_search_name").data('description');
            var product_price = $("#product_search_name").data('price');
            var product_qnt = $("#product_search_name").data('qnt');
            var qnt = $("#product_qntd").val();
            var status = !empty(product_id) && !empty(product_description) ? true : false;

            if(status) {
                var has_product = $("#stock_list tr td.stock_item_description[data-id="+product_id+"]").length;

                if(has_product) {
                    var row = $("tbody#stock_list tr td.stock_item_description[data-id="+product_id+"]").parents('tr');
                    var new_qnt = Number(row.children('td.stock_item_qnt').text()) + Number(qnt);

                    if(product_qnt >= new_qnt)
                        row.children('td.stock_item_qnt').text(new_qnt);
                    else {
                        UIkit.notify({
                            message : 'a quantidade definida: <strong>'+new_qnt+'</strong>, excedeu a quantidade em estoque: <strong>'+product_qnt+'</strong>',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });

                        return false;
                    }
                }
                else{
                    var product_total = empty(product_price) ? '' : format_number( (Number(qnt) * Number(product_price) ).toFixed(2) );
                    var id = $("#stock_list tr").length ? Number($("#stock_list tr:last").data('id')) + 1 : Number(window.item_last) + 1;

                    if(product_qnt == 0) {
                        UIkit.notify({
                            message : 'O produto não está disponível em estoque!',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                        return false;
                    }

                    if(product_qnt < qnt) {
                        UIkit.notify({
                            message : 'a quantidade definida: <strong>'+qnt+'</strong>, excedeu a quantidade em estoque: <strong>'+product_qnt+'</strong>',
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                        return false;
                    }

                    stock_list.add(id, product_id, product_description, product_qnt, qnt, format_number((product_price).toFixed(2)), product_total);
                    window.item_last = id;
                }

                $("#product_search_name")
                                         .data('id', '')
                                         .data('description', '')
                                         .data('price', '')
                                         .data('qnt', '')
                                         .val('').blur();
                $("#product_qntd").val('').blur();
                $("#amount").val(format_number(calc_amount(),'BRL')).blur();
            }
        });

        // remove product
        $(document).on('click', '.delete',function () {
            $(this).parents('tr').remove();
            $("#amount").val(format_number(calc_amount())).blur();
        });

        // calc itens
        $("body").on('DOMSubtreeModified', ".stock_item_qnt", function () {
            var row = $(this).parents('tr');
            var stock_qnt = row.data('qnt');
            var new_qnt = row.children('.stock_item_qnt').text();

           if(Number(stock_qnt) == 0) {
               UIkit.notify({
                   message : 'este produto não está mais disponível em estoque',
                   status  : 'danger',
                   timeout : 5000,
                   pos     : 'bottom-center'
               });

               row.remove();
           }
           else if(Number(stock_qnt) < Number(new_qnt)) {
                UIkit.notify({
                    message : 'a quantidade definida: <strong>'+new_qnt+'</strong>, excedeu a quantidade em estoque: <strong>'+stock_qnt+'</strong>',
                    status  : 'danger',
                    timeout : 5000,
                    pos     : 'bottom-center'
                });

                row.children('.stock_item_qnt').text(stock_qnt);
            }
        });

        // calc itens
        $("body").on('DOMSubtreeModified', ".stock_item_qnt, .stock_item_price", function () {
            var row = $(this).parents('tr');
            var qnt = row.children('.stock_item_qnt').text();
            var price = row.children('.stock_item_price').text();

            if(!empty(qnt) && !empty(price)){
                price = price.replace('.', '');
                price = price.replace(',', '.');
                var amount = (Number(qnt) * Number(price)).toFixed(2);

                row.children('.stock_item_amount').text(format_number(amount));
            }
            else {
                row.children('.stock_item_amount').text('');
            }

        });

        // calc amount
        $("body").on('DOMSubtreeModified', ".stock_item_amount", function () {
            var amount = calc_amount();

            $("#amount").val(format_number(amount)).blur();
        });

        // calc discount
        $(document).on('ifChanged', '.discount_type', function () {
            calc_discount();
        });
        $(document).on('change', '#discount', function () {
            calc_discount();
        });
    },
    add : function (id, product_id, product_description, stock_qnt, product_qnt, product_price, product_total) {
        var row = '<tr data-id="'+id+'" data-qnt="'+stock_qnt+'">' +
                         '<td class="stock_item_description" data-id="'+product_id+'">'+product_description+'</td>' +
                         '<td class="stock_item_qnt number" contenteditable="true">'+product_qnt+'</td>' +
                         '<td class="stock_item_price" contenteditable="true">'+product_price+'</td>' +
                         '<td class="stock_item_amount">'+product_total+'</td>' +
                         '<td class="hidden-print"> <a class="delete" href="#"><i class="md-icon material-icons">delete</i></a></td>' +
                  '</tr>';

        $("tbody#stock_list").append(row);
    },
    remove : function (id) {
        $("tbody#stock_list tr[data-id="+id+"]").remove();
    },
    enabled : function () {
        $(".stock_item_qnt, .stock_item_price").prop('contenteditable', true);
        $("#add_product, .delete, #provider, #discount, .discount_type, textarea#obs").prop('disabled', false);
        $(".discount_type").parents('.iradio_md').removeClass('disabled');

        $("#edit").hide();
        $("#save").show();
        $("#finish").show();
    },
    disabled : function () {
        $(".stock_item_qnt, .stock_item_price").prop('contenteditable', false);
        $("#add_product, .delete, #provider, #discount, .discount_type, textarea#obs").prop('disabled',true);
        $(".discount_type").parents('.iradio_md').addClass('disabled');

        $("#save").hide();
        $("#edit").show();
        $("#finish").show();
    },
    finish : function () {
        $(".stock_item_qnt, .stock_item_price").prop('contenteditable', false);
        $("#add_product, .delete, #provider, #discount, .discount_type, #product_search_name, #product_qntd, textarea#obs").prop('disabled',true);
        $(".discount_type").parents('.iradio_md').addClass('disabled');

        $("#edit").hide();
        $("#save").hide();
        $("#finish").hide();
    },
    status : function () {
        var status = $("#stock_list").data('status');

        if(status == 'editable')
            this.enabled();
        else if(status == 'preview')
            this.disabled();
        else if(status == 'finish')
            this.finish();
    }
}

function get_itens() {
    var itens = [];

    $("tbody#stock_list tr").each(function (i, element) {
        var row = $(element);
        var id = row.data('id');
        var order = $("tbody#stock_list").data('id');
        var product = row.children('.stock_item_description').data('id');
        var description = row.children('.stock_item_description').text();
        var price = row.children('.stock_item_price').text();
        var qnt = row.children('.stock_item_qnt').text();

        itens.push({'id' : String(id), 'order' : String(order), 'product' : String(product), 'description' : description, 'price' : (Number(format_number(price,'USD'))).toFixed(2), 'qnt': qnt});
    });

    return itens;
}

function get_order() {
    var provider = $("#provider").val();
    var discount = $("#discount").val();
    var discount_type = $(".discount_type:checked").val();
    var obs = $("#obs").val();;
    var type = $("#stock_list").data('type');

    return {'provider' : provider, 'discount' : (Number(format_number(discount, 'USD'))).toFixed(2), 'discount_type' : discount_type, 'obs': obs, 'type' : String(type)};
}

function calc_amount() {
    var amount = '';

    $("#stock_list .stock_item_amount").each(function (i, element) {
        var price = $(element).text();
        amount = (Number(amount) + Number(format_number(price, 'USD'))).toFixed(2);
    });

    return amount;
}

function calc_discount() {
    var amount = calc_amount();
    var discount = $("#discount").val();
    var discount_type = $(".discount_type:checked").val();
    var new_amount = null;

    if(amount && discount) {
        if(discount_type == 'real') {
            new_amount = ( amount - parseFloat(format_number(discount,'USD')) ).toFixed(2);
            $("#amount").val(format_number(new_amount));
        }
        else if(discount_type == 'por') {
            discount = ( amount * (parseFloat(format_number(discount,'USD'))/100) ).toFixed(2);
            new_amount = (amount - discount).toFixed(2);
            $("#amount").val(format_number(new_amount));
        }
    }
    else
        $("#amount").val(amount);
}

function save(order_status) {
        order_status = empty(order_status) ? 'pending' : order_status;
    var order_id = $("#stock_list").data('id');
    var order = new Array();
        order.orgin = window.order;
        order.now = get_order();
        order.change = diff_JSON(window.order, order.now);

    var itens = new Array();
        itens.origin = window.item_list;
        itens.now = get_itens();
        itens.change = {'add': [], 'del': [], 'update' : []};

    $.each(itens.now, function (i, item_now) {
        var item_origin = search_array(item_now.id, 'id', itens.origin);

        if(item_origin) {
            var changes = diff_JSON(item_origin, item_now);
            if(!empty(changes))
                itens.change.update.push({'id' : item_origin.id, 'changes' : changes});
        }
        else
            itens.change.add.push(item_now);
    });
    $.each(itens.origin, function (i, item_origin) {
        var item_del = search_array(item_origin.id, 'id', itens.now);

        if(!item_del)
            itens.change.del.push(item_origin);
    });


    if(empty($("#provider").val())) {
        UIkit.notify({
            message : 'é necessário ter um cliente definido para salvar o pedido!',
            status  : 'danger',
            timeout : 5000,
            pos     : 'bottom-center'
        });
        return false;
    }

    if(order_status == 'finish')
    {
        if($("tbody#stock_list tr").length == 0) {
            UIkit.notify({
                message : 'é necessário ter itens adicionados para finalizar o pedido!',
                status  : 'danger',
                timeout : 5000,
                pos     : 'bottom-center'
            });
            return false;
        }
    }


    var status = ( !empty(order.change) || ( !empty(itens.change.add) || !empty(itens.change.update) || !empty(itens.change.del)) ) ? true : false;

    if(status || order_status == 'finish') {
        altair_helpers.content_preloader_show('md');

        $.ajax({
             url: base_url('stock/save'),
            type: 'POST',
            data: {'order_id' : order_id, 'order' : order.change, 'itens' : itens.change, 'order_status' : order_status},
        dataType: 'json',
         success: function (data) {
            altair_helpers.content_preloader_hide();

            console.log(data);

            if(data.status == 1) {

                if(data.response) {
                    if(!empty(data.response.hasOwnProperty('order'))){
                        $("#stock_list").data('id',data.response.order);
                        window.order = get_order();
                    }

                    if(!empty(data.response.hasOwnProperty('itens_list'))) {
                        $.each(data.response.itens_list, function (i, item) {
                            var target = $("#stock_list tr[data-id="+item.target+"]");
                            target.data('id', item.id);
                            delete item['target'];
                            window.item_list.push(item);
                        });

                        var last = data.response.itens_list.length - 1;
                        window.item_last = Number(data.response.itens_list[last].id) + 1;
                    }
                }

                if(!empty(order.change)) {
                    $.each(order.change, function (key, value) {
                        window.order[key] = value;
                    });
                }

                if(!empty(itens.change.update)) {
                    $.each(itens.change.update, function (i, item_now) {
                        $.each(window.item_list, function (index, item_origin) {
                            if(item_origin.id == item_now.id) {
                                if(item_now.changes.hasOwnProperty('price'))
                                    item_origin.price = item_now.changes.price;

                                if(item_now.changes.hasOwnProperty('qnt'))
                                    item_origin.qnt = item_now.changes.qnt;

                                window.item_list[index] = item_origin;
                            }
                        })
                    });
                }

                if(!empty(itens.change.del)) {
                    $.each(itens.change.del, function (i, item_now) {
                        $.each(window.item_list, function (index, item_origin) {
                            if(empty(item_origin))
                                return;

                            if(item_origin.id == item_now.id) {
                                window.item_list.splice(index,1);
                                return;
                            }
                        })
                    });
                }

                var action = order_status == 'finish' ? 'finish' : 'disabled';
                eval('stock_list.'+action+'()');
            }
            else {
                $.each(data.message, function (i, message) {
                    UIkit.notify({
                        message : message.error,
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                });
            }
         },
           error: function () {
            altair_helpers.content_preloader_hide();

            UIkit.notify({
                message : 'An error occurred on the server! :(',
                status  : 'danger',
                timeout : 5000,
                pos     : 'bottom-center'
            });
           }
        });
    }
    else
        stock_list.disabled();
}