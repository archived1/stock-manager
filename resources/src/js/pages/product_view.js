$(function () {
    // product form
    product_form.init();

    // Select provider
    $.each(window.price_list, function (i, price) {
        var select_id = "provider__"+price.id;
        var select = new Select($("#"+select_id)).init();
        var current_provider = price.provider;

        select.load_list(window.provider_list)
              .select(price.provider);

        $.each(window.provider_list, function (i, option) {
            if(option.value == current_provider)
                window.provider_list[i].status = false;
        }, current_provider);

        window.select_provider[select_id] = current_provider;
    });
    $(document).on('focus', 'select.provider_select', function () {
        var $this = $(this);
        var select = new Select($this).init();
        var select_id = $this.attr('id');
        window.select_provider[select_id] = this.value;

        select.empty();
        $.each(window.provider_list, function (i, option) {
            if(option.status)
                select.add(option.value, option.text);
        }, select);
    });
    $(document).on('change', 'select.provider_select', function () {
        var select_id = $(this).attr('id');
        var new_provider = this.value;
        var current_provider = window.select_provider[select_id];

        $.each(window.provider_list, function (i, option) {
            if(option.value == current_provider)
                window.provider_list[i].status = true;

            else if(option.value == new_provider)
                window.provider_list[i].status = false;
        }, current_provider, new_provider);

        window.select_provider[select_id] = new_provider;
    });

    // delete product
    $("#delete_product").click(function () {
        var product_id = $("#product_id").val();
        var product_name = $.trim($("#product_form input#description").val());

        UIkit.modal.confirm('Tem certeza que deseja remover o produto '+product_name+' ?', function(){

            altair_helpers.content_preloader_show('md');
            $.ajax({
                 url: base_url('product/delete'),
                type: 'POST',
                data: {'product_id' : product_id},
            dataType: 'json',
             success: function (data) {
                    altair_helpers.content_preloader_hide();

                    if(data.status == 1)
                        redirect(base_url('product'));
                    else {
                        UIkit.notify({
                            message : data.message,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                },
               error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'Connection fail',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });
        });

        return false;
    });

    // edit product
    $("#edit_product").click(function () {
        product_form.enabled();
    });

    // save product
    $("#save_product").click(function () {
        var product = new Array();
        product.now = $("#product_form").serializeObject(function(value){
            if($(value).prop('name') == 'price')
                return (Number(format_number($(value).val(),'USD'))).toFixed(2);
            else
                return $(value).val();
        });
        product.origin = window.product;
        product.change = diff_JSON(product.origin, product.now);

        var prices = new Array();
        prices.now = get_providers();
        prices.origin = window.price_list;
        prices.change = {'add': [], 'del': [], 'update' : []};

        $.each(prices.now, function (i, price_now) {
            var price_origin = search_array(price_now.id, 'id', prices.origin);

            if(price_origin) {
                var changes = diff_JSON(price_origin, price_now);
                if(!empty(changes))
                    prices.change.update.push({'id' : price_origin.id, 'changes' : changes});
            }
            else
                prices.change.add.push(price_now);
        });
        $.each(prices.origin, function (i, price_origin) {
            var price_del = search_array(price_origin.id, 'id', prices.now);

            if(!price_del)
                prices.change.del.push(price_origin);
        });

        var status = ( !empty(product.change) || (!empty(prices.change.add) || !empty(prices.change.del) || !empty(prices.change.update)) ) ? true : false;

        console.log(prices);

        if(status) {
            altair_helpers.content_preloader_show('md');

            $.ajax({
                    url: base_url('product/save'),
                   type: 'POST',
                   data: {'product_id' : product.origin.id, 'product' : product.change, 'price_by_provider' : prices.change},
               dataType: 'json',
                success: function (data) {
                        altair_helpers.content_preloader_hide();

                        if(data.status == 1) {
                            if(data.response) {
                                if(!empty(data.response.hasOwnProperty('product'))) {
                                    $("#product_id").val(data.response.product);
                                    window.product = product.now;
                                }

                                if(!empty(data.response.hasOwnProperty('price_list'))) {
                                    if(!empty(data.response.price_list)) {

                                        $.each(data.response.price_list, function (i, price) {
                                            var target = $("#price_by_provider .wrapper_dynamic_fields[data-id=" + price.target + "]");
                                            target.find('label[for=provider__' + price.target + ']').prop('for', 'provider__' + price.id);
                                            target.find('select#provider__' + price.target)
                                                .prop('name', 'provider__' + price.id)
                                                .prop('id', 'provider__' + price.id);

                                            target.find('label[for=price__' + price.target + ']').prop('for', 'price__' + price.id);
                                            target.find('input#price__' + price.target)
                                                .prop('name', 'price__' + price.id)
                                                .prop('id', 'price__' + price.id);
                                            delete price['target'];
                                            window.price_list.push(price);
                                        });

                                        var last = data.response.price_list.length - 1;
                                        var index = Number(data.response.price_list[last].id) + 1;
                                        $('#price_by_provider').prop('dynamic-fields-counter', index);
                                    }
                                }
                            }

                            if(!empty(product.change)) {
                                $.each(product.change, function (key, value) {
                                    window.product[key] = value;
                                });
                            }

                            if(!empty(prices.change.update)) {
                                $.each(prices.change.update, function (i, price_now) {
                                    $.each(window.price_list, function (index, price_origin) {
                                        if(price_origin.id == price_now.id) {
                                            if(price_now.changes.hasOwnProperty('price'))
                                                price_origin.price = price_now.changes.price;

                                            if(price_now.changes.hasOwnProperty('provider'))
                                                price_origin.provider = price_now.changes.provider;

                                            window.price_list[index] = price_origin;
                                            return;
                                        }
                                    })
                                });
                            }

                            if(!empty(prices.change.del)) {
                                $.each(prices.change.del, function (i, price_now) {
                                    $.each(window.price_list, function (index, price_origin) {
                                        if(empty(price_origin))
                                            return;

                                        if(price_origin.id == price_now.id) {
                                            window.price_list.splice(index,1);
                                            return;
                                        }
                                    })
                                });
                            }

                            product_form.disabled();
                        }
                        else {
                            $.each(data.message, function (i, message) {
                                UIkit.notify({
                                    message : message.error,
                                    status  : 'danger',
                                    timeout : 5000,
                                    pos     : 'bottom-center'
                                });
                            });
                        }
                    },
                  error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                  }
            });
        }
        else
            product_form.disabled();
    });
});

$(document).on('click', '.btnSectionRemove', function () {
    var $this = window.dynamic_fields;
    var select_id = $this.find('select.provider_select').attr('id');
    var current_provider = $this.find('select.provider_select option:selected').val();
    window.select_provider.splice(select_id,1);

    $.each(window.provider_list, function (i, option) {
        if(option.value == current_provider)
            window.provider_list[i].status = true;
    }, current_provider);
});

product_form = {
    init: function () {
        var status = $("#product_form").data('edit');

        if(status)
            this.enabled();
        else
            this.disabled();
    },
    enabled: function () {
        $("form#product_form input").prop('disabled', false);
        $("form#product_form select").prop('disabled', false);
        $("#price_by_provider input").prop('disabled', false);
        $("#price_by_provider select").prop('disabled', false);
        $("a.btnSectionRemove").prop('disabled', false);
        $("a.btnSectionClone").prop('disabled', false);
        $("#save_product").show();
        $("#edit_product").hide();
    },
    disabled: function () {
        $("form#product_form input").prop('disabled', true);
        $("form#product_form select").prop('disabled', true);
        $("#price_by_provider input").prop('disabled', true);
        $("#price_by_provider select").prop('disabled', true);
        $("a.btnSectionRemove").prop('disabled', true);
        $("a.btnSectionClone").prop('disabled', true);
        $("#save_product").hide();
        $("#edit_product").show();
    }
}

function Select(selector) {
    this.selector = selector;

    this.init = function () {
        return this
    }

    this.enable = function () {
        this.selector.prop("disabled", false).parents('.md-input-wrapper').removeClass('md-input-wrapper-disabled');

        return this;
    }

    this.disable = function () {
        this.selector.prop("disabled", true).parents('.md-input-wrapper').addClass('md-input-wrapper-disabled');

        return this;
    }

    this.load_list = function (list) {
        var $this = this;

        $.each(list, function (i, option) {
            $this.add(option.value, option.text);
        },$this);

        return this;
    }

    this.select = function (value) {
        this.selector.children('option[value="'+value+'"]').prop('selected', true);

        return this;
    }

    this.options = function () {
        var o = [];

        this.selector.children().each(function (i, option) {
            if($(option).val()) {
                var option = {value: $(option).val(), text: $(option).text()};
                o.push(option);
            }
            else
                return;
        });

        return o;
    }

    this.add = function (value, text) {
        var option = $("<option/>", {value: value,text: text});
        this.selector.append(option);

        return this;
    }

    this.remove = function (value) {
        this.selector.children('option[value="'+value+'"]').remove();

        return this;
    }

    this.empty = function () {
        this.selector.children('option:not([value=""]):not(:selected)').remove();

        return this;
    }

    return this;
}

function get_providers() {
    var o = [];

    $(".wrapper_dynamic_fields").each(function (i, element) {
        var price_id = $(element).data('id');
        var provider_id = $(element).find('select.provider_select').val();
        var product_id = $('#product_id').val();
        var price_value = $(element).find('input.money').val();

        if(!empty(provider_id))
            o.push({'id' : String(price_id), 'provider' : String(provider_id), 'product' : String(product_id), 'price' : (Number(format_number(price_value,'USD'))).toFixed(2)});
    });

    return o;
}