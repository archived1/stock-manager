$(function() {
    // Init common functions on document ready
    lists = {
        city: new Selectize('city_list').init(),
        uf:  new Selectize('uf_list').init()
    };
    buttons_actions.init();
    client_form.init();

    //
    lists.uf.instance().on('change', function () {
        lists.city.disable()
                  .empty();

        var uf_id = lists.uf.instance().getValue();

        if(!empty(window.city_list[uf_id])) {
            lists.city.load_list(window.city_list[uf_id])
                      .enable();
        }
        else {
            $.ajax({
                url: base_url('service/city'),
                type: 'POST',
                data: {id:uf_id},
                dataType: 'json',
                success: function (data) {
                    lists.city.load_list(data)
                              .enable();
                    window.city_list[uf_id] = data;
                },
                error: function () {
                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });
        }
    });

    // submit profile
    $("#client_form").submit(function () {
        var client = $("#client_form").serializeObject(function (value) {
            var status = $(value).hasEvent(null,'mask');

            if(!empty(status))
                return $(value).cleanVal();
            else
                return $(value).val()
        });

        altair_helpers.content_preloader_show('md');
        $.ajax({
            url: base_url('client/save'),
            type: 'POST',
            data: client,
            dataType: 'json',
            success: function (data) {
                altair_helpers.content_preloader_hide();

                if(data.status == 1) {
                    $("#client_id").val(data.client_id);

                    client['id'] = data.client_id;
                    window.client_profile = client;

                    $("#submit_profile").hide();
                    $("#actions_profile").show();

                    $(document).trigger('disable_form');
                }
                else {
                    UIkit.notify({
                        message : data.mensagem,
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }

            },
            error: function () {
                altair_helpers.content_preloader_hide();

                UIkit.notify({
                    message : 'An error occurred on the server! :(',
                    status  : 'danger',
                    timeout : 5000,
                    pos     : 'bottom-center'
                });
            }
        });

        return false;
    });

    // save profile
    $(document).on('click', 'div#client_actions button#save', function () {
        var data_now = $("#client_form").serializeObject(function (value) {
            var status = $(value).hasEvent(null,'mask');

            if(!empty(status))
                return $(value).cleanVal();
            else
                return $(value).val()
        });
        var data_origin = window.client_profile;
        var data_change = diff_JSON(data_origin,data_now);

        if(!empty(data_change)) {
            altair_helpers.content_preloader_show('md');

            $.ajax({
                url: base_url('client/edit'),
                type: 'POST',
                data: {'id': data_origin.id, 'data':data_change},
                dataType: 'json',
                success: function (data) {
                    altair_helpers.content_preloader_hide();

                    if(data.status != 1) {
                        UIkit.notify({
                            message : data.mensagem,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }

                },
                error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });
        }

        $(document).trigger('disable_form');
    });

    // edit profile
    $(document).on('click', 'div#client_actions button#edit', function () {
        $(document).trigger('enable_form');
    });

    // print profile
    $(document).on('click', 'div#client_actions button#print', function () {
        window.print();
    });

    // remove profile
    $(document).on('click', 'div#client_actions button#delete', function () {
        var id = $("#client_id").val();
        var name = $("#trade").val();

        UIkit.modal.confirm('Tem certeza que deseja remover o fornecedor '+name+'?', function(){
            altair_helpers.content_preloader_show('md');
            $.ajax({
                url: base_url('client/delete'),
                type: 'POST',
                data: {id:id},
                dataType: 'json',
                success: function (data) {
                    altair_helpers.content_preloader_hide();

                    if(data.status == 1)
                        redirect(base_url('client'));
                    else {
                        UIkit.notify({
                            message : data.mensagem,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                },
                error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });
        });

        return false;
    });
});

buttons_actions = {
    init: function () {
        $(document).on('enable_form.buttons_actions', function () {
            buttons_actions.save();
        });
        $(document).on('disable_form.buttons_actions', function () {
            buttons_actions.edit();
        });
    },
    stop: function () {
        $(document).off('enable_form.buttons_actions');
        $(document).off('disable_form.buttons_actions');
    },
    save: function () {
        $("div#client_actions button#save").remove();
        $("div#client_actions button#edit").remove();
        $("div#client_actions").prepend('<button id="save" type="button" data-uk-tooltip="{cls:\'uk-tooltip-small\',pos:\'bottom\'}" title="Save"><i class="material-icons md-color-white">save</i></button>');
    },
    edit: function () {
        $("div#client_actions button#edit").remove();
        $("div#client_actions button#save").remove();
        $("div#client_actions").prepend('<button id="edit" type="button" data-uk-tooltip="{cls:\'uk-tooltip-small\',pos:\'bottom\'}" title="Edit"><i class="material-icons md-color-white">create</i></button>');
    }
}

client_form = {
    instance: function () {
        return $("#client_form");
    },
    init: function () {
        $(document).on('enable_form.client_form', function () {
            client_form.enable();
        });
        $(document).on('disable_form.client_form', function () {
            client_form.disable();
        });

        $('#trade').on('focusout', function () {
            var value = $(this).val();

            $("#client_show_trade").text(value);
        });
        $('#owner').on('focusout', function () {
            var value = $(this).val();

            $("#client_show_owner").text(value);
        });

        var status = this.instance().data('edit');

        if(status)
            $(document).trigger('enable_form');
        else
            $(document).trigger('disable_form');
    },
    stop: function () {
        $(document).off('enable_form.client_form');
        $(document).off('disable_form.client_form');

        $('#trade').off('focusout');

    },
    enable: function () {
        $("form#client_form input").prop('disabled', false);
        $("form#client_form select").prop('disabled', false);
        lists.city.enable();
        lists.uf.enable();
    },
    disable: function () {
        $("form#client_form input").prop('disabled', true);
        $("form#client_form select").prop('disabled', true);
        lists.city.disable();
        lists.uf.disable();
    },
    serialize: function () {
        return this.instance().serializeObject();
    }
}

function Selectize(selector) {
    this.selector = selector;
    
    this.instance = function () {
        var selector = this.selector;
        var $selector = $("#"+selector+"").selectize();
        return $selector[0].selectize;
    }

    this.init = function () {
        this.instance();

        if(empty(this.options()))
            this.disable();

        return this;
    }
    
    this.enable = function () {
        var $this = this.instance();
        $this.enable();

        return this;
    }
    
    this.disable = function () {
        var $this = this.instance();
        $this.disable();

        return this;
    }

    this.load_list = function (list) {
        var $this = this.instance();
        $.each(list,function (id,value) {
            $this.addOption({value:value.id,text:value.name});
            $this.addItem(value,true);
        });
        $this.refreshOptions();
        $this.refreshItems();

        return this;
    }
    
    this.select = function (id) {
        var $this = this.instance();
        $this.setValue(id);

        return this;
    }
    
    this.add = function (value, text) {
        var $this = this.instance();
        $this.addOption({value:value,text:text})
             .refreshOptions();
        $this.addItem(value,true)
             .refreshItems();

        return this;
    }

    this.remove = function (id) {
        var $this = this.instance();
        $this.removeOption(id, true);

        return this;
    }

    this.empty = function () {
        var $this = this.instance();
        this.refresh();
        $this.clearOptions();

        return this;
    }

    this.refresh = function () {
        var $this = this.instance();
        $this.clear();

        return this;
    }

    this.options = function () {
        var $this = this.instance();
        return $this.options;
    }
}