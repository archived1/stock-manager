$(function () {
    form_validation_change_password.change_password();

    $(document).on('click','#modal_change_password button#send',function () {
        $("#modal_change_password form").submit(function () {
            altair_helpers.content_preloader_show('md');
            $("#modal_change_password button#send").prop("disabled",true).addClass('disabled');

            $.ajax({
                url: base_url('authentication/change_password'),
                type: 'POST',
                data: $("#modal_change_password form").serialize(),
                dataType: 'json',
                success: function (data) {
                    altair_helpers.content_preloader_hide();
                    $("#modal_change_password button#send").prop("disabled",false).removeClass('disabled');
                    console.log(data);

                    if(data.status == 1) {
                        UIkit.modal("#modal_change_password").hide();

                        UIkit.notify({
                            message : data.message,
                            status  : 'success',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                    else {
                        UIkit.notify({
                            message : data.message,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                },
                error: function () {
                    altair_helpers.content_preloader_hide();
                    $("#modal_change_password button#send").prop("disabled",false).removeClass('disabled');

                    UIkit.notify({
                        message : 'Connection fail',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'top-center'
                    });
                }
            });

            return false;
        }).trigger('submit');
    });

    $("body").on('DOMSubtreeModified', "#notifications-indicator", function () {
        var indicator = Number($(this).text());

        if(indicator >= 1) {
            $(this).show();
            $("#notification-empty").hide();
        }
        else
            $(this).hide();
    });

    $("#sidebar_main_toggle").click(function () {
       var collapse = $('body').hasClass('sidebar_mini');
       var sidebar = collapse ? 'sidebar_mini' : 'sidebar_main_open';

        $.ajax({
            url: base_url('service/setting'),
            type: 'POST',
            data: {'sidebar' : sidebar},
            dataType: 'json'
        });
    });
});


// validation (parsley)
form_validation_change_password = {
    change_password: function() {
        var $formValidate = $('#modal_change_password form');

        $formValidate
            .parsley()
            .on('form:validated', function () {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated', function (parsleyField) {
                if ($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input($(parsleyField.$element));
                }
            });
    }
};

function notify(message, title, type) {
    type = empty(type) ? 'primary' : type;
    var type_list = {
                        'success' : '&#xE88F;',
                        'warning' : '&#xE8B2;',
                        'danger' : '&#xE001;',
                        'primary' : '&#xE001;'
                     };
    var indicator = Number($(this).text()) + 1;

    var $row = '<li>' +
                    '<div class="md-list-addon-element">' +
                        '<i class="md-list-addon-icon material-icons uk-text-'+type+'">'+type_list[type]+'</i>' +
                    '</div>' +
                    '<div class="md-list-content">' +
                        '<span class="md-list-heading">'+title+'</span>' +
                        '<span class="uk-text-small uk-text-muted uk-text-truncate">'+message+'</span>' +
                    '</div>' +
               '</li>';
    $("#notifications-body").append($row);
    $("#notifications-indicator").text(indicator);
}