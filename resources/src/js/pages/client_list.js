$(function() {
    //
    search_client.init();

    // client list
    $(document).on('click', 'a.delete', function () {
        var id = $(this).parents('tr').data("id");
        var name = $.trim($(this).parents('tr').children('td.provider_trade').text());

        UIkit.modal.confirm('Tem certeza que deseja remover o cliente '+name+'?', function(){

            altair_helpers.content_preloader_show('md');
            $.ajax({
                    url: base_url('client/delete'),
                   type: 'POST',
                   data: {id:id},
               dataType: 'json',
                success: function (data) {
                    altair_helpers.content_preloader_hide();

                    if(data.status == 1)
                        $('tr[data-id='+id+']').remove();
                    else {
                        UIkit.notify({
                            message : data.mensagem,
                            status  : 'danger',
                            timeout : 5000,
                            pos     : 'bottom-center'
                        });
                    }
                },
                  error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'Connection fail',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });
        });

        return false;
    });
});


search_client = {
    init : function () {
        //search
        $("#submit_search_client").on('click',function () {
            window.search_keyword = $("#name_search_client").val();

            if(empty(window.search_keyword))
                return false;

            altair_helpers.content_preloader_show('md');
            $.ajax({
                url: base_url('client/search'),
                type: 'POST',
                data: {search: window.search_keyword},
                dataType: 'json',
                success: function (data) {
                    if(data.status) {
                        search_client.clear();

                        $.each(data.results, function (key, value) {
                            search_client.add(value);
                        });

                        search_client.pagination(data.pagination);

                        search_client.show();
                    }
                    else
                        search_client.show_error();

                    altair_helpers.content_preloader_hide();
                },
                error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });

            return false;
        });

        $("#name_search_client").on('focusout', function () {
            var value = $(this).val();

            if(empty(value))
                search_client.hide();
        });

        $("#search_client").submit(function () {
           return false;
        });

        //
        $(document).on('click', '#result_list .uk-pagination li a', function () {
            var cur_page = $(this).data('ci-pagination-page');

            altair_helpers.content_preloader_show('md');
            $.ajax({
                url: base_url('client/search'),
                type: 'POST',
                data: {'search': window.search_keyword, 'cur_page': cur_page},
                dataType: 'json',
                success: function (data) {
                    altair_helpers.content_preloader_hide();

                    if(data.status) {
                        search_client.clear();

                        console.log(data);

                        $.each(data.results, function (key, value) {
                            search_client.add(value);
                        });

                        search_client.pagination(data.pagination);
                    }
                    else {
                        search_client.show_error();
                    }

                },
                error: function () {
                    altair_helpers.content_preloader_hide();

                    UIkit.notify({
                        message : 'An error occurred on the server! :(',
                        status  : 'danger',
                        timeout : 5000,
                        pos     : 'bottom-center'
                    });
                }
            });

            return false;
        });

        // back search
        $(document).on('click', '#back_search_client', function () {
            $("#name_search_client").val('');
            search_client.hide();
        });
    },
    show : function () {
        $("#client_list").hide();
        $("#result_error").hide();
        $("#result_list").show();
    },
    show_error: function () {
        $("#client_list").hide();
        $("#result_list").hide();
        $("#result_error").show();
    },
    hide : function () {
        $("#result_list").hide();
        $("#result_error").hide();
        $("#client_list").show();
        this.clear();
    },
    add : function (data) {
        var $row = '<tr data-id="'+data.id+'">' +
                        '<td class="provider_trade">'+data.trade+'</td>' +
                        '<td class="phone">'+data.phone+'</td>' +
                        '<td>' +
                            '<a class="view" href="'+base_url('client/view/'+data.id)+'"><i class="md-icon material-icons">visibility</i></a>' +
                            '<a class="edit" href="'+base_url('client/view/'+data.id+'/edit')+'"><i class="md-icon material-icons">create</i></a>' +
                            '<a class="delete" href="#"><i class="md-icon material-icons">delete</i></a>' +
                        '</td>' +
                    '</tr>';
        $("#result_list table tbody").append($row);
    },
    pagination: function (data) {
        $("#result_list > div").append(data);
    },
    clear: function () {
        $("#result_list table tbody").html('');
        $("#result_list > div").html('');
    }
}