// -------------------- load dependencies gulp --------------------
var del          = require('del');
var gulp         = require('gulp');
var concat       = require('gulp-concat');
var rename       = require('gulp-rename');
var babel        = require('gulp-babel');
var plumber      = require('gulp-plumber');
var uglify       = require('gulp-uglify');
var less         = require('gulp-less');
var cssmin       = require('gulp-cssmin');
var sourcemaps   = require('gulp-sourcemaps');
var prefixer     = require('gulp-autoprefixer');
var imagemin     = require('gulp-imagemin');
var environments = require('gulp-environments');
var browserSync  = require('browser-sync').create();


// -------------------- variables --------------------------------

// variables of environment
var pkg   = require('./package.json');
var bower = require('./bower.json');
var production  = environments.production;
var development = environments.development;

// base directories path
var path =
{
    public: '../public',
    views:  '../application/views',
    tests:  './tests',
    src:    './src',
    vendor: './vendor',
    dist:   function() {
        return development() ? this.tests : this.public;
    }
}

// assets path
var assets =
{
    img:    path.dist() + '/assets/img',
    css:    path.dist() + '/assets/css',
    js:     path.dist() + '/assets/js',
    fonts:  path.dist() + '/assets/fonts',
    vendor: path.dist() + '/assets/vendor'
}

// source path
var source =
{
    html: {
        path:  path.src + '/html',
        files: path.src + '/html/**'
    },
    less: {
        path:  path.src + '/less',
        files: path.src + '/less/**/*.less'
    },
    js: {
        path:  path.src + '/js',
        files: path.src + '/js/**/*.js'
    },
    img: {
        path:  path.src + '/img',
        files: path.src + '/img/**/*.{jpg,png,gif,svg}'
    },
    fonts: {
        path:  path.src + '/fonts',
        files: path.src + '/fonts/*.{ttf,woff,eof,svg}'
    }
}


// -------------------- setting of environment --------------------------

// set environment for development
gulp.task('set-env-dev', function(done){
    environments.current(development);
    done();
});

// set environment for production
gulp.task('set-env-prod', function(done){
    environments.current(production);
    done();
});

// show current environment
gulp.task('env-current', function(done){
    var status = production() ? 'production' : development() ? 'development' : undefined;
    console.log('current environment is ' + status);
    done();
});


// -------------------- clean directorys --------------------------

// clean up the image subdirectory of the assets directory
gulp.task('clean-img', function(done){
    var file = '/**';
    var not = '!';
    del.sync([assets.img+file, not+assets.img],{force:true});
    done();
});

// clean up the css subdirectory of the assets directory
gulp.task('clean-css', function(done){
    var file = '/**';
    var not = '!';
    del.sync([assets.css+file, not+assets.css],{force:true});
    done();
});

// clean up the js subdirectory of the assets directory
gulp.task('clean-js', function(done){
    var file = '/**';
    var not = '!';
    del.sync([assets.js+file, not+assets.js],{force:true});
    done();
});

// clean up the fonts subdirectory of the assets directory
gulp.task('clean-fonts', function(done){
    var file = '/**';
    var not = '!';
    del.sync([assets.fonts+file, not+assets.fonts],{force:true});
    done();
});

// clean up the vendor subdirectory of the assets directory
gulp.task('clean-vendor', function(done){
    var file = '/**';
    var not = '!';
    del.sync([assets.vendor+file, not+assets.vendor],{force:true});
    done();
});

// clean up the assets directory
gulp.task('clean-all', ['clean-img', 'clean-css', 'clean-js', 'clean-fonts', 'clean-vendor']);

// clean up the tests directory
gulp.task('clean-tests', function(done){
    var file = '/**';
    var not = '!';
    del.sync([path.tests+file, not+path.tests],{force:true});
    done();
});


// -------------------- build source ---------------------

// load views files
gulp.task('views', function(){
    return gulp.src(source.html.files)
               //.pipe(production(rename({extname: ".php"})))
               //.pipe(production(gulp.dest(path.views)))
               .pipe(development(gulp.dest(path.tests)));
});

// compile file less login
gulp.task('compile-less-page-login', function () {
    return gulp.src(source.less.path + '/pages/login.less')
               .pipe(plumber())
               .pipe(development(sourcemaps.init()))
               .pipe(less())
               .pipe(prefixer({browsers: ['> 1%', 'last 2 versions', 'firefox >= 4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11']}))
               .pipe(production(cssmin()))
               .pipe(rename({suffix:'.min'}))
               .pipe(sourcemaps.write('.'))
               .pipe(gulp.dest(assets.css))
               .pipe(browserSync.stream());
});

// compile less files
gulp.task('compile-less-main', function () {
    return gulp.src(source.less.path + '/main.less')
               .pipe(plumber())
               .pipe(development(sourcemaps.init()))
               .pipe(less())
               .pipe(prefixer({browsers: ['> 1%', 'last 2 versions', 'firefox >= 4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11']}))
               .pipe(production(cssmin()))
               .pipe(production(rename({suffix:'.min'})))
               .pipe(development(sourcemaps.write('.')))
               .pipe(gulp.dest(assets.css))
               .pipe(browserSync.stream());
});

// compile less code
gulp.task('compile-less',['compile-less-page-login', 'compile-less-main']);

// compile javascript files
gulp.task('compile-js', function(){
    return gulp.src([source.js.files, '!' + source.js.path + '/vendor/*.js'])
               .pipe(plumber())
               .pipe(babel())
               //.pipe(production(uglify()))
               .pipe(rename({suffix:'.min'}))
               .pipe(gulp.dest(assets.js))
               .pipe(browserSync.stream());
});

// optimizing and load images files
gulp.task('img', function(){
    return gulp.src([source.img.files, '!' + source.img.path + '/icons/**'])
               .pipe(imagemin())
               .pipe(gulp.dest(assets.img))
               .pipe(browserSync.stream());
});

// load fonts files
gulp.task('fonts', function(){
    return gulp.src(source.fonts.files)
               .pipe(gulp.dest(assets.fonts))
               .pipe(browserSync.stream());
});

// watch the changes in source files
gulp.task('watch-source', ['views', 'compile-less-main', 'compile-less-page-login', 'compile-js', 'build-vendor-js', 'img', 'fonts'], function(){
    gulp.watch(source.html.files, ['views']);
    gulp.watch([!source.less.path + '/pages/**', source.less.files], ['compile-less-main']);
    gulp.watch([
            source.less.path + '/_variables_mixins.less',
            source.less.path + '/_altair_admin.less',
            source.less.path + '/md/**.less',
            source.less.path + '/plugins/_icheck.less',
            source.less.path + '/components/_preloaders.less',
            source.less.path + '/pages/login_page.less'],
            ['compile-less-page-login']);
    gulp.watch([source.js.files, '!' + source.js.path + '/vendor/*.js'], ['compile-js']);
    gulp.watch(source.js.path + '/vendor/*.js', ['build-vendor-js']);
    gulp.watch(source.img.files,   ['img']);
    gulp.watch(source.fonts.files, ['fonts']);
});

// build all source files
gulp.task('build-source', ['views', 'compile-less-main', 'compile-less-page-login', 'compile-js', 'img', 'fonts']);


// -------------------- build dependencies -----------------------

// build commmon.js
gulp.task('build-common-js', function () {
    return gulp.src([
        path.vendor + "/jquery/dist/jquery.js",
        path.vendor + "/modernizr/modernizr.js",
        // moment
        path.vendor + "/moment/moment.js",
        // retina images
        path.vendor + "/dense/src/dense.js",
        // fastclick (touch devices)
        path.vendor + "/fastclick/lib/fastclick.js",
        // custom scrollbar
        path.vendor + "/jquery.scrollbar/jquery.scrollbar.js",
        // create easing functions from cubic-bezier co-ordinates
        path.vendor + "/jquery-bez/jquery.bez.min.js",
        // Get the actual width/height of invisible DOM elements with jQuery
        path.vendor + "/jquery.actual/jquery.actual.js",
        // waypoints
        path.vendor + "/waypoints/lib/jquery.waypoints.js",
        // velocityjs (animation)
        path.vendor + "/velocity/velocity.js",
        path.vendor + "/velocity/velocity.ui.js",
        // advanced cross-browser ellipsis
        path.vendor + "/jquery.dotdotdot/src/js/jquery.dotdotdot.js",
        // jQuery inputMask
        path.vendor + "/jquery-mask-plugin/dist/jquery.mask.js",
        // iCheck
        path.vendor + "/iCheck/icheck.js",
        // parsley
        path.vendor + "/parsleyjs/dist/parsley.js",
        // selectize
        path.vendor + "/selectize/dist/js/standalone/selectize.js",
        // switchery
        path.vendor + "/switchery/dist/switchery.js",
        // textarea-autosize
        path.vendor + "/autosize/dist/autosize.js",
        // hammerjs
        path.vendor + "/hammerjs/hammer.js",
        // scrollbar width
        source.js.path+"/custom/jquery.scrollbarWidth.js",
        // jquery.debouncedresize
        path.vendor + "/jquery.debouncedresize/js/jquery.debouncedresize.js",
        // screenfull
        path.vendor + "/screenfull/dist/screenfull.js",
        // waves
        path.vendor + "/Waves/dist/waves.js"
    ])
               .pipe(development(sourcemaps.init()))
               .pipe(concat('common.min.js'))
               //.pipe(production(uglify()))
               .pipe(development(sourcemaps.write('.')))
               .pipe(gulp.dest(assets.vendor))
               .pipe(browserSync.stream());
});

// load of dependencies
gulp.task('load-dependencies', function () {
    return gulp.src([
                    path.vendor + '/datatables/media/js/jquery.dataTables.js',
                    path.vendor + '/datatables-colvis/js/dataTables.colVis.js',
                    path.vendor + '/datatables-tabletools/js/dataTables.tableTools.js',
                    path.vendor + '/handlebars/handlebars.js',
                    path.vendor + '/clndr/src/clndr.js',
                    path.vendor + '/countUp.js/dist/countUp.js'
                   ])
               //.pipe(production(uglify()))
               .pipe(rename({suffix:'.min'}))
               .pipe(gulp.dest(assets.vendor))
               .pipe(browserSync.stream());
});

// build vendor js
gulp.task('build-vendor-js', function () {
    return gulp.src(source.js.path + "/vendor/*.js")
               .pipe(plumber())
               .pipe(babel({
                   "env": {
                       "development" : {
                           "compact": false
                       }
                   }
               }))
               //.pipe(production(uglify()))
               .pipe(rename({suffix:'.min'}))
               .pipe(gulp.dest(assets.vendor))
               .pipe(browserSync.stream());
});

// dependencies uikit
gulp.task('uikit-css', function () {
    return gulp.src(path.vendor + '/uikit/css/uikit.almost-flat.css')
               .pipe(plumber())
               .pipe(production(cssmin()))
               .pipe(prefixer())
               .pipe(rename({suffix:'.min'}))
               .pipe(gulp.dest(assets.vendor + '/uikit/css'))
               .pipe(browserSync.stream());
});
gulp.task('uikit-js', function () {
    return gulp.src([
                    // uikit core
                    path.vendor + "/uikit/js/core/core.js",
                    path.vendor + "/uikit/js/core/touch.js",
                    path.vendor + "/uikit/js/core/utility.js",
                    path.vendor + "/uikit/js/core/smooth-scroll.js",
                    path.vendor + "/uikit/js/core/scrollspy.js",
                    path.vendor + "/uikit/js/core/toggle.js",
                    path.vendor + "/uikit/js/core/alert.js",
                    path.vendor + "/uikit/js/core/button.js",
                    path.vendor + "/uikit/js/core/dropdown.js",
                    path.vendor + "/uikit/js/core/grid.js",
                    source.js.path + "/custom/uikit_modal.js",
                    path.vendor + "/uikit/js/core/nav.js",
                    path.vendor + "/uikit/js/core/offcanvas.js",
                    path.vendor + "/uikit/js/core/switcher.js",
                    path.vendor + "/uikit/js/core/tab.js",
                    path.vendor + "/uikit/js/core/cover.js",

                    // uikit components
                    path.vendor + "/uikit/js/components/accordion.js",
                    path.vendor + "/uikit/js/components/autocomplete.js",
                    source.js.path + "/custom/uikit_datepicker.js",
                    path.vendor + "/uikit/js/components/form-password.js",
                    path.vendor + "/uikit/js/components/form-select.js",
                    path.vendor + "/uikit/js/components/grid.js",
                    path.vendor + "/uikit/js/components/lightbox.js",
                    path.vendor + "/uikit/js/components/nestable.js",
                    path.vendor + "/uikit/js/components/notify.js",
                    path.vendor + "/uikit/js/components/sortable.js",
                    source.js.path + "/custom/uikit_sticky.js",
                    path.vendor + "/uikit/js/components/tooltip.js",
                    source.js.path + "/custom/uikit_timepicker.js",
                    path.vendor + "/uikit/js/components/upload.js",
                    source.js.path + "/custom/uikit_beforeready.js"
                ])
               .pipe(concat('uikit.min.js'))
               .pipe(production(uglify()))
               .pipe(gulp.dest(assets.vendor+'/uikit/js'))
               .pipe(browserSync.stream());
});
gulp.task('uikit-fonts', function(){
    gulp.src(path.vendor + "/uikit/fonts/*.{ttf,woff,woff2,eof,svg}")
        .pipe(gulp.dest(assets.vendor + '/uikit/fonts'))
});
gulp.task('uikit', ['uikit-css', 'uikit-js', 'uikit-fonts']);

// dependencies jQuery UI
gulp.task('jquery-ui-css', function () {
    return gulp.src(path.vendor + '/jquery-ui/themes/base/jquery-ui.min.css')
        .pipe(gulp.dest(assets.vendor + '/jquery-ui/css'))
        .pipe(browserSync.stream());
});
gulp.task('jquery-ui-js', function () {
    return gulp.src( path.vendor + "/jquery-ui/jquery-ui.min.js")
        .pipe(gulp.dest(assets.vendor + '/jquery-ui/js'))
        .pipe(browserSync.stream());
});
gulp.task('jquery-ui', ['jquery-ui-css', 'jquery-ui-js']);

// dependencies weathericons
gulp.task('weathericons-css', function () {
    return gulp.src(path.vendor + '/weather-icons/css/weather-icons.css')
               .pipe(plumber())
               .pipe(production(cssmin()))
               .pipe(prefixer())
               .pipe(rename({suffix:'.min'}))
               .pipe(gulp.dest(assets.vendor + '/weathericons/css'))
               .pipe(browserSync.stream());
});
gulp.task('weathericons-fonts', function () {
    return gulp.src(path.vendor + '/weather-icons/font/**')
               .pipe(gulp.dest(assets.vendor + '/weathericons/font'))
               .pipe(browserSync.stream());
});
gulp.task('weathericons',['weathericons-css','weathericons-fonts']);

// dependencies material design icons
gulp.task('material-design-icons', function () {
    return gulp.src(path.vendor + "/material-design-icons-iconfont/dist/fonts/*.{ttf,woff,woff2,eof,svg}")
               .pipe(gulp.dest(assets.vendor + "/material-design-icons"))
});

// build all dependencies
gulp.task('build-all-dependencies', ['build-common-js', 'load-dependencies', 'build-vendor-js', 'uikit', 'jquery-ui', 'weathericons', 'material-design-icons']);


// --------------------  hooks  -----------------------

// build app
gulp.task('build', ['build-all-dependencies', 'build-source']);

// set up a local testing server
gulp.task('server', ['clean-tests', 'set-env-dev', 'build-all-dependencies', 'build-source'], function(){
    browserSync.init({
        server:{
            baseDir: path.tests
        }
    });

    gulp.watch(source.html.files, ['views']).on('change',browserSync.reload);
    gulp.watch(source.less.files, ['compile-less']);
    gulp.watch([source.js.files, '!' + source.js.path + '/vendor/*.js'], ['compile-js']);
    gulp.watch(source.js.path + '/vendor/*.js', ['build-vendor-js']);
    gulp.watch(source.img.files, ['img']);
    gulp.watch(source.fonts.files, ['fonts']);
});