<?php

$section = strtolower($this->uri->segment(1));
$sections['dashboard']  = '';
$sections['provider']   = '';
$sections['client']   = '';
$sections['product']    = '';
$sections['stock']    = '';

if(array_key_exists($section, $sections))
    $sections[$section] = 'current_section';
?>

<!-- main sidebar -->
<aside id="sidebar_main">
    <div class="menu_section">
        <ul>
            <li id="dashboard" title="Dashboard" class="<?= $sections['dashboard']; ?>">
                <a href="<?= base_url('dashboard'); ?>">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>
            </li>

            <li id="fornecedores" title="Fornecedores" class="<?= $sections['provider']; ?>">
                <a href="<?= base_url('provider'); ?>">
                    <span class="menu_icon"><i class="material-icons">business</i></span>
                    <span class="menu_title">Fornecedores</span>
                </a>
            </li>

            <li id="clientes" title="Clientes" class="<?= $sections['client']; ?>">
                <a href="<?= base_url('client'); ?>">
                    <span class="menu_icon"><i class="material-icons">supervisor_account</i></span>
                    <span class="menu_title">Clientes</span>
                </a>
            </li>

            <li id="produtos" title="Produtos" class="<?= $sections['product']; ?>">
                <a href="<?= base_url('Product'); ?>">
                    <span class="menu_icon"><i class="material-icons">shopping_basket</i></span>
                    <span class="menu_title">Produtos</span>
                </a>
            </li>

            <li id="estoque" title="Estoque" class="<?= $sections['stock']; ?>">
                <a href="<?= base_url('stock'); ?>">
                    <span class="menu_icon"><i class="material-icons">ballot</i></span>
                    <span class="menu_title">Estoque</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<!-- main sidebar end -->