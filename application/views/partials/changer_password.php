<!-- Modal Changer Password -->
<div class="uk-modal" id="modal_change_password">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">CHANGE PASSWORD</h3>
        </div>

        <form action="" class="uk-form-stacked" >
            <div class="uk-grid" >
                <div class="uk-width-1-1">
                    <div class="parsley-row">
                        <label for="name">Current Password<span class="req">*</span></label>
                        <input type="password" id="currentPassword" name="currentPassword" data-parsley-trigger="change" required  class="md-input" />
                    </div>
                </div>
            </div>
            <div class="uk-grid" >
                <div class="uk-width-1-2">
                    <div class="parsley-row">
                        <label for="password">New Password<span class="req">*</span></label>
                        <input type="password" id="newPassword" name="newPassword" data-parsley-trigger="change" required  class="md-input" />
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="parsley-row">
                        <label for="confirmPassword">Confirm Password<span class="req">*</span></label>
                        <input type="password" id="confirmPassword" name="confirmPassword" data-parsley-equalto="#newPassword" data-parsley-trigger="change" required  class="md-input" />
                    </div>
                </div>
            </div>
        </form>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" id="close" class="md-btn md-btn-flat uk-modal-close">Cancel</button>
            <button type="button" id="send" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
    </div>
</div>
<!-- Modal Changer Password -->