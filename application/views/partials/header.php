<!-- main header -->
<header id="header_main">
    <div class="header_main_content">

        <!-- navbar-top -->
        <nav class="uk-navbar">

            <!-- logo navbar top -->

            <!--
            <div class="main_logo_top">
                <a href="#"><h3 class="md-color-blue-grey-50">SLR Tool</h3></a>
            </div>
            -->

            <!-- logo navbar top end -->

            <!-- button toggle siderbar -->
            <div class="uk-float-left">
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
            </div>
            <!-- button toggle siderbar -->

            <!-- actions bar -->
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li><a href="#" class="user_action_icon" id="full_screen_toggle" full-screen-toggle> <i class="material-icons md-24 md-light">&#xE5D0;</i> </a></li>
                    <li><main-search-show></main-search-show></li>
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}" id="notifications">
                        <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge" id="notifications-indicator" style="display: none">0</span></a>
                        <div class="uk-dropdown uk-dropdown-large">
                            <div class="md-card-content">
                                <ul class="md-list md-list-addon" id="notifications-body">
                                    <li id="notification-empty">
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons ">&#xE88F;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">Notificações</span>
                                            <span class="uk-text-small uk-text-muted uk-text-truncate">Não há notificações</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <!-- dropdown user -->
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}" >
                        <a href="#" class="user_action_image"> <i class="material-icons md-36 md-light">account_circle</i> </a>
                        <div class="uk-dropdown uk-dropdown-small uk-dropdown-bottom" style="min-width: 160px; top: 47px; left: 62px;">
                            <ul class="uk-nav js-uk-prevent">
                                <li><a href="#" data-uk-modal="{target:'#modal_change_password'}">Changer Password</a></li>
                                <li><a href="<?= base_url('logout'); ?>">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                    <!-- dropdown user -->
                </ul>
            </div>
            <!-- actions bar -->

        </nav>
        <!-- navbar-top end -->

    </div>
</header>
<!-- main header end -->