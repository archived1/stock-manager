<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="pt-br"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->template->meta; ?>
    <?= $this->template->title->set('Login'); ?>
    <?= $this->template->stylesheet
                                   ->add_first('http://fonts.googleapis.com/css?family=Roboto:300,400,500')
                                   ->add_first('assets/css/login.min.css')
                                   ->add_first('assets/vendor/uikit/css/uikit.almost-flat.min.css','all')
    ;?>
</head>
<body>

    <div class="login_page">
        <div class="md-card">
            <div class="md-card-content">
                <div class="login_heading">
                    <img src="assets/img/logo.png" id="logo">
                </div>
                <form id="login" method="post" action="#">
                    <div class="uk-form-row">
                        <label for="username">Username</label>
                        <input class="md-input" type="text" id="username" name="username" required />
                    </div>
                    <div class="uk-form-row">
                        <label for="password">Password</label>
                        <input class="md-input" type="password" id="password" name="password" required />
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit"  class="md-btn md-btn-primary md-btn-block md-btn-large">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?= $this->template->javascript
                                   ->add('assets/js/pages/login.min.js')
                                   ->add_first('assets/js/helpers.min.js')
                                   ->add_first('assets/vendor/altair_admin_common.min.js')
                                   ->add_first('assets/vendor/uikit/js/uikit.min.js')
                                   ->add_first('assets/vendor/common.min.js')
    ;?>

</body>
</html>