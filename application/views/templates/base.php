<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="pt-br"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->template->meta; ?>
    <?= $this->template->title; ?>
    <?= $this->template->stylesheet
                                   ->add_first('http://fonts.googleapis.com/css?family=Roboto:300,400,500')
                                   ->add_first('assets/css/main.min.css','all')
                                   ->add_first('assets/vendor/uikit/css/uikit.almost-flat.min.css','all')
                                   ->add('assets/vendor/weathericons/css/weather-icons.min.css','all')
    ;?>
</head>
<?php $sidebar = $this->session->has_userdata('sidebar') ? $this->session->userdata('sidebar') : 'sidebar_main_open'; ?>
<body class="header_full <?= $sidebar; ?>">

    <?= $this->template->content
                                ->section('header') // set a new section of elements
                                ->add('partials/header')
                                ->add('partials/sidebar')
                                ->first() // fixed  current section(header section) on top
                                ->section('footer') // set a new section of elements
                                ->add('partials/changer_password')
                                ->last()
    ;?>

    <?= $this->template->javascript
                                   ->add_first('assets/vendor/altair_admin_common.min.js')
                                   ->add_first('assets/js/helpers.min.js')
                                   ->add_first('assets/vendor/uikit_custom.min.js')
                                   ->add_first('assets/vendor/common.min.js')
                                   ->add('assets/js/pages/base.min.js');
    ;?>


    <?php $events = get_current_events();?>
    <?php if(!empty($events)): ?>
    <script>
        $(function () {
            <?php foreach ($events as $event): ?>
            <?php $date = date('d-m-Y', strtotime($event['date'])); ?>
            <?php $message = "{$date} às {$event['timeStart']}"; ?>
            notify('<?=$message?>','<?=$event['title'];?>','primary');
            <?php endforeach; ?>
        });
    </script>
    <?php endif;?>

    <?php if($this->session->flashdata('notify')): ?>
        <script>
            $(function () {
                var notify_list = <?=json_encode($this->session->flashdata('notify'));?>;
                $.each(notify_list, function (i, notify) {
                    if(empty(notify.mensagem))
                        return false;

                    UIkit.notify({
                        timeout : 5000,
                        message : notify.mensagem,
                        status  : notify.status,
                        pos     : notify.position
                    });
                });
            });
        </script>
    <?php endif;?>
</body>
</html>