<?php
//load dependencies
$this->template->javascript
                           ->add('assets/js/pages/stock_list.min.js');
?>

<!-- page content start -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="md-card uk-width-large-7-10 uk-container-center">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-large-3-10 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Start Date</label>
                            <input class="md-input" type="text" id="uk_dp_start">
                        </div>
                    </div>

                    <div class="uk-width-large-3-10 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">End Date</label>
                            <input class="md-input" type="text" id="uk_dp_end">
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <div class="uk-margin-small-top">
                            <select id="stock_status" data-md-selectize>
                                <option value="">Status</option>
                                <?php if(!empty($status_list)):?>
                                    <?php foreach ($status_list as $key => $status):?>
                                        <option value="<?= $key; ?>" ><?= $status; ?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10 uk-text-center">
                        <button id="submit_search" class="md-btn md-btn-primary uk-margin-small-top">Buscar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-card uk-margin-medium-bottom uk-width-large-7-10 uk-container-center">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                            <li class="uk-width-1-2"><a href="#" data-type="1">ENTRADA</a></li>
                            <li class="uk-width-1-2"><a href="#" data-type="2">SAÍDA</a></li>
                        </ul>

                        <ul id="tabs_1" class="uk-switcher uk-margin">
                            <!-- entrada start -->
                            <li id="stock_entry">
                                <table class="uk-table uk-table-nowrap">
                                    <?php if(!empty($stock_entry)): ?>
                                    <thead>
                                        <th class="uk-width-1-10 ">Código</th>
                                        <th class="uk-width-4-10 ">Fornecedor</th>
                                        <th class="uk-width-2-10 ">Data</th>
                                        <th class="uk-width-2-10 ">Status</th>
                                        <th class="uk-width-1-10 small_col"></th>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($stock_entry as $stock): ?>
                                        <tr data-id="<?= $stock['id']; ?>">
                                            <td class="stock_code"><?= $stock['id']; ?></td>
                                            <td class="stock_provider">
                                                <?= $stock['trade']; ?>
                                            </td>
                                            <td class="stock_date">
                                                <?= date('d/m/Y', strtotime($stock['date'])); ?>
                                            </td>
                                            <td class="stock_status" data-status="<?= $stock['status']; ?>">
                                                <?= $status_list[$stock['status']] ?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url("stock/entry/{$stock['id']}"); ?>" class="view"><i class="md-icon material-icons">visibility</i></a>

                                              	<?php if($stock['status'] != 2): ?>
                                                <a href="<?= base_url("stock/entry/{$stock['id']}/edit"); ?>" class="edit"><i class="md-icon material-icons">edit</i></a>
                                            	<?php endif; ?>

                                                <a href="#" class="delete"><i class="md-icon material-icons">delete</i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                <?php else:?>
                                    <h3 class="uk-text-center uk-margin-top">Não há registros de entrada de estoque!</h3>
                                <?php endif; ?>
                                </table>
                                <div id="pagination_entry" class="wrapper_pagination" data-type="1">
                                    <?php
                                    if(!empty($pagination['entry']))
                                        echo $pagination['entry'];
                                    ?>
                                </div>


                                <div class="result_list" style="display: none">
                                    <table class="uk-table uk-table-nowrap">
                                        <thead>
                                            <th class="uk-width-1-10 ">Código</th>
                                            <th class="uk-width-4-10 ">Fornecedor</th>
                                            <th class="uk-width-2-10 ">Data</th>
                                            <th class="uk-width-2-10 ">Status</th>
                                            <th class="uk-width-1-10 small_col"></th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div class="wrapper_pagination" data-type="1">
                                    </div>
                                </div>
                                <div class="result_error" style="display: none">
                                    <h3 class="uk-text-center uk-margin-top">Nenhum resultado foi encontrado!</h3>
                                    <div class="uk-flex uk-flex-center">
                                        <a href="#" class="uk-position-relative back_search" data-type="entry">Voltar</a>
                                    </div>
                                </div>
                            </li>
                            <!-- entrada end -->

                            <!-- saída start-->
                            <li id="stock_out">
                                <table class="uk-table uk-table-nowrap">
                                    <?php if(!empty($stock_out)): ?>
                                        <thead>
                                        <th class="uk-width-1-10 ">Código</th>
                                        <th class="uk-width-4-10 ">Cliente</th>
                                        <th class="uk-width-2-10 ">Data</th>
                                        <th class="uk-width-2-10 ">Status</th>
                                        <th class="uk-width-1-10 small_col"></th>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($stock_out as $stock): ?>
                                            <tr data-id="<?= $stock['id']; ?>">
                                                <td class="stock_code"><?= $stock['id']; ?></td>
                                                <td class="stock_provider">
                                                    <?= $stock['trade']; ?>
                                                </td>
                                                <td class="stock_date">
                                                    <?= date('d/m/Y', strtotime($stock['date'])); ?>
                                                </td>
                                                <td class="stock_status" data-status="<?= $stock['status']; ?>">
                                                    <?= $status_list[$stock['status']]; ?>
                                                </td>
                                                <td>
                                                    <a href="<?= base_url("stock/out/{$stock['id']}"); ?>" class="view"><i class="md-icon material-icons">visibility</i></a>
                                                    
                                                    <?php if($stock['status'] != 2): ?>
                                                    <a href="<?= base_url("stock/out/{$stock['id']}/edit"); ?>" class="edit"><i class="md-icon material-icons">edit</i></a>
                                                    <?php endif; ?>

                                                    <a href="#" class="delete"><i class="md-icon material-icons">delete</i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    <?php else:?>
                                        <h3 class="uk-text-center uk-margin-top">Não há registros de saída de estoque!</h3>
                                    <?php endif; ?>
                                </table>
                                <div id="pagination_out" class="wrapper_pagination" data-type="2">
                                    <?php
                                    if(!empty($pagination['out']))
                                        echo $pagination['out'];
                                    ?>
                                </div>


                                <div class="result_list" style="display: none">
                                    <table class="uk-table uk-table-nowrap">
                                        <thead>
                                        <th class="uk-width-1-10 ">Código</th>
                                        <th class="uk-width-4-10 ">Fornecedor</th>
                                        <th class="uk-width-2-10 ">Data</th>
                                        <th class="uk-width-2-10 ">Status</th>
                                        <th class="uk-width-1-10 small_col"></th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div class="wrapper_pagination" data-type="2">
                                    </div>
                                </div>
                                <div class="result_error" style="display: none">
                                    <h3 class="uk-text-center uk-margin-top">Nenhum resultado foi encontrado!</h3>
                                    <div class="uk-flex uk-flex-center">
                                        <a href="#" class="uk-position-relative back_search" data-type="out">Voltar</a>
                                    </div>
                                </div>
                            </li>
                            <!-- saida end-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-grid uk-position-bottom-right uk-position-fixed uk-margin-bottom uk-margin-right" data-uk-grid-margin>
            <a class="md-fab md-fab-danger md-fab-wave-light waves-effect waves-button waves-light uk-margin-right" href="<?= base_url('stock/entry'); ?>">
                <i class="material-icons">arrow_downward</i>
            </a>

            <a class="md-fab md-fab-danger md-fab-wave-light waves-effect waves-button waves-light" href="<?= base_url('stock/out'); ?>">
                <i class="material-icons">arrow_upward</i>
            </a>
        </div>

    </div>
</div>
<!-- page content end -->