<?php
//load dependencies
$this->template->javascript
                           ->add('assets/vendor/jquery-ui/js/jquery-ui.min.js')
                           ->add('assets/js/pages/stock_out.min.js');
$this->template->stylesheet
                           ->add('assets/vendor/jquery-ui/css/jquery-ui.min.css');
?>

<!-- page content start -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="md-card uk-width-large-5-10 uk-container-center hidden-print">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-6-10">
                        <label for="product_search_name">Descrição de Produto</label>
                        <input type="text" class="md-input" id="product_search_name" data-id="" data-description="" data-price="" data-qnt="">
                    </div>

                    <div class="uk-width-medium-2-10">
                        <label for="product_qntd">Qntd</label>
                        <input type="text" class="md-input" id="product_qntd">
                    </div>

                    <div class="uk-width-medium-2-10 uk-text-center">
                        <button id="add_product" class="md-btn md-btn-primary uk-margin-small-top">Add</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-card uk-width-large-7-10 uk-container-center">
            <div class="md-card-content">
                <table class="uk-table uk-table-nowrap">
                    <thead>
                        <th class="uk-width-4-10 ">Descrição de Produto</th>
                        <th class="uk-width-2-10 ">Quantidade</th>
                        <th class="uk-width-2-10 ">Preço</th>
                        <th class="uk-width-2-10 ">Total</th>
                        <th class="uk-width-1-10 hidden-print"></th>
                    </thead>
                    <tbody id="stock_list" data-id="<?= $order['id']; ?>" data-status="<?= $status; ?>" data-type="<?= $order['type']; ?>">
                    <?php if(!empty($item_list)): ?>
                        <?php foreach ($item_list as $item): ?>
                            <tr data-id="<?= $item['id']; ?>" data-qnt="<?= $item['stock_qnt']; ?>">
                                <td class="stock_item_description" data-id="<?= $item['product']; ?>">
                                    <?= $item['description']; ?>
                                </td>
                                <td class="stock_item_qnt number" contenteditable="true">
                                    <?= $order['status'] == 2 ? $item['qnt'] : ($item['stock_qnt'] >= $item['qnt'] ? $item['qnt'] : 0); ?>
                                </td>
                                <td class="stock_item_price" contenteditable="true">
                                    <?= number_format((float)$item['price'], 2, ',', '.'); ?>
                                </td>
                                <td class="stock_item_amount money">
                                    <?= number_format((float)($item['qnt'] * $item['price']), 2, ',', '.'); ?>
                                </td>
                                <td class="hidden-print">
                                    <a class="delete" href="#"><i class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>

                <div class="uk-grid uk-margin-large-top" data-uk-grid-margin="">

                    <div class="uk-width-medium-4-10">
                        <label for="provider">Cliente</label>

                        <?php if($order['status'] == 2):?>
                            <input type="text" class="md-input" id="provider" value="<?= $order['company']; ?>">
                        <?php else:?>
                            <select name="provider" id="provider" class="md-input">
                                <option value=""></option>
                                <?php if(!empty($provider_list)):?>
                                    <?php foreach ($provider_list as $provider):?>
                                        <?php $select = ($provider['id'] == $order['provider'] ? 'selected' : ''); ?>
                                        <option value="<?=$provider['id'];?>" <?=$select?> ><?=$provider['trade'];?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        <?php endif;?>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <label for="discount">Desconto</label>
                        <input type="text" class="md-input money" id="discount" name="discount" value="<?= number_format((float)$order['discount'], 2, ',', '.'); ?>">
                    </div>

                    <div class="uk-width-medium-2-10 uk-position-relative">
                        <div class="uk-position-bottom" >
                            <?php
                                $discount_type['real'] = $order['discount_type'] == 'real' ? 'checked' : '';
                                $discount_type['por'] =  $order['discount_type'] == 'por' ? 'checked' : '';

                                if(empty($order['discount_type']))
                                    $discount_type['real'] = 'checked';
                            ?>
                            <div style="display: table; margin: auto">
                                <span class="icheck-inline" style="margin-bottom: 0px !important;">
                                    <input type="radio" name="discount_type" id="discount_type_1" class="discount_type" value="real" <?=$discount_type['real'];?>/>
                                    <label for="discount_type_2" class="inline-label">R$</label>
                                </span>
                                <span class="icheck-inline" style="margin-bottom: 0px !important;">
                                    <input type="radio" name="discount_type" id="discount_type_2" class="discount_type" value="por" <?=$discount_type['por'];?>/>
                                    <label for="discount_type_1" class="inline-label">%</label>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <label for="amount">Valor Total</label>
                        <input type="text" class="md-input" id="amount" name="amount" value="" disabled="disabled">
                    </div>

                    <div class="uk-width-1-1">
                        <div class="uk-form-row">
                            <label>Observação</label>
                            <textarea cols="30" rows="1" id="obs" name="obs" class="md-input"><?=$order['obs'];?></textarea>
                        </div>
                    </div>
                </div>

                <div class="uk-flex uk-flex-center uk-margin-top hidden-print">
                    <button id="print" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Imprimir</button>
                    <button id="edit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" style="display: none">Editar</button>
                    <button id="save" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" style="display: none">Salvar</button>
                    <button id="finish" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light" style="display: none">Finalizar</button>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- page content end -->