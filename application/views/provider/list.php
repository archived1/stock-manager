<?php
//load dependencies
$this->template->javascript->add('assets/js/pages/provider_list.min.js');
?>

<!-- page content -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="search-full uk-margin-small-top uk-margin-medium-bottom uk-width-large-4-10 uk-container-center">
            <form class="search-content" id="search_provider" action="#">
                <div class="uk-input-group">
                    <span class="uk-input-group-addon uk-padding-remove" id="submit_search_provider">
                        <i class="md-icon material-icons">search</i>
                    </span>
                    <input type="search" id="name_search_provider" name="name_search_provider" placeholder="Pesquisar por fornecedor">
                </div>
            </form>
        </div>

        <div class="md-card uk-margin-medium-bottom uk-width-large-6-10 uk-container-center">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <div id="provider_list">
                        <table class="uk-table uk-table-nowrap">
                            <?php if(!empty($providers)): ?>
                                <thead>
                                    <th class="uk-width-3-10 ">Fornecedor</th>
                                    <th class="uk-width-2-10 ">Contato</th>
                                    <th class="uk-width-1-10 small_col"></th>
                                </thead>
                                <tbody>
                                <?php foreach ($providers as $provider): ?>
                                    <tr data-id="<?= $provider['id']; ?>" >
                                        <td class="provider_trade">
                                            <?= $provider['trade']; ?>
                                        </td>
                                        <td class="phone">
                                            <?= $provider['phone']; ?>
                                        </td>
                                        <td>
                                            <a class="view" href="<?= base_url("provider/view/{$provider['id']}"); ?>"><i class="md-icon material-icons">visibility</i></a>
                                            <a class="edit" href="<?= base_url("provider/view/{$provider['id']}/edit"); ?>"><i class="md-icon material-icons">create</i></a>
                                            <a class="delete" href="#"><i class="md-icon material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            <?php else:?>
                                <h3 class="uk-text-center uk-margin-top">Não há fornecedores cadastrados!</h3>
                            <?php endif; ?>
                        </table>

                        <?php
                        if(!empty($pagination))
                            echo $pagination;
                        ?>
                    </div>

                    <div id="result_list" style="display: none">
                        <table class="uk-table uk-table-nowrap">
                            <thead>
                                <th class="uk-width-3-10 ">Fornecedor</th>
                                <th class="uk-width-2-10 ">Contato</th>
                                <th class="uk-width-1-10 small_col"></th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div></div>
                    </div>
                    <div id="result_error" style="display: none">
                        <h3 class="uk-text-center uk-margin-top">Nenhum resultado foi encontrado!</h3>
                        <div class="uk-flex uk-flex-center">
                            <a href="#" class="uk-position-relative" id="back_search_provider">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave waves-effect waves-button" href="<?= base_url('provider/create'); ?>">
                <i class="material-icons">add</i>
            </a>
        </div>

    </div>
</div>
<!-- page content end -->