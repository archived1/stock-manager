<?php
//load dependencies
$this->template->javascript->add('assets/js/pages/client_profile.min.js');
?>

<!-- page content -->
<div id="page_content">
    <div id="page_content_inner">

        <form action="#" class="uk-form-stacked " id="client_form" data-edit="<?= $edit; ?>" method="post">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-6-10 uk-container-center">
                    <div class="md-card">


                        <input type="hidden" name="id" id="client_id" value="<?= $client['id']; ?>" >

                        <div class="user_heading hidden-print" data-uk-sticky="{ top: 48, media: 960 }">
                            <div class="user_heading_avatar fileinput fileinput-new">
                                <div class="fileinput-new thumbnail">
                                    <img src="<?= base_url('assets/img/cliente.png'); ?>">
                                </div>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b">
                                    <span class="uk-text-truncate" id="client_show_trade"><?= $client['trade']; ?></span>
                                    <span class="sub-heading" id="client_show_owner"><?= $client['owner']; ?></span>
                                </h2>
                            </div>

                            <?php
                                $buttons_actions = $create_profile ? 'style="display:none"' : '';
                                $button_save = $create_profile ? '' : 'style="display:none"';
                            ?>
                            <div class="md-fab-wrapper" <?= $buttons_actions; ?> id="actions_profile">
                                <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                    <i class="material-icons">&#xE8BE;</i>
                                    <div id="client_actions" class="md-fab-toolbar-actions">
                                        <button id="print" type="button" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Print"><i class="material-icons md-color-white">print</i></button>
                                        <button id="delete" type="button" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Delete"><i class="material-icons md-color-white">delete</i></button>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="submit_profile" class="md-fab md-fab-small md-fab-accent" <?= $button_save; ?> >
                                <i class="material-icons">save</i>
                            </button>
                        </div>

                        <div class="user_content">
                            <h3 class="full_width_in_card heading_c">Geral</h3>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="trade">Nome Fantasia</label>
                                        <input class="md-input" type="text" id="trade" name="trade" value="<?= $client['trade']; ?>" required>
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="company">Razão Social</label>
                                        <input class="md-input" type="text" id="company" name="company" value="<?= $client['company']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="cnpj">CNPJ</label>
                                        <input class="md-input cnpj" type="text" id="cnpj" name="cnpj" value="<?= $client['cnpj']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="owner">Proprietário</label>
                                        <input class="md-input" type="text" id="owner" name="owner" value="<?= $client['owner']; ?>" required>
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                            </div>

                            <h3 class="full_width_in_card heading_c">Contato</h3>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="email">e-mail</label>
                                        <input class="md-input" type="email" id="email" name="email" value="<?= $client['email']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="phone">celular</label>
                                        <input class="md-input phone" type="text" id="phone" name="phone" value="<?= $client['phone']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                            </div>

                            <h3 class="full_width_in_card heading_c">Endereço</h3>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper">
                                        <label for="rua">rua</label>
                                        <input class="md-input" type="text" id="street" name="street" value="<?= $client['street']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-2-10">
                                    <div class="md-input-wrapper">
                                        <label for="n">nº</label>
                                        <input class="md-input" type="text" id="n" name="n" value="<?= $client['n']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-3-10">
                                    <div class="md-input-wrapper">
                                        <label for="cep">cep</label>
                                        <input class="md-input cep" type="text" id="cep" name="cep" value="<?= $client['cep']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-3-10">
                                    <div class="md-input-wrapper">
                                        <label for="bairro">bairro</label>
                                        <input class="md-input" type="text" id="district" name="district" value="<?= $client['district']; ?>">
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <select id="city_list" name="city" required >
                                        <option value="">Cidade</option>
                                        <?php if(!empty($city_list)):?>
                                            <?php foreach ($city_list as $city):?>
                                                <?php $select = ($client['city'] == $city['id'] ? 'selected' : ''); ?>
                                                <option value="<?=$city['id'];?>" <?=$select?>><?=$city['name'];?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-10">
                                    <select id="uf_list" name="uf" required >
                                        <option value="">UF</option>
                                        <?php if(!empty($uf_list)):?>
                                            <?php foreach ($uf_list as $uf):?>
                                                <?php $select = ($client['uf'] == $uf['id'] ? 'selected' : ''); ?>
                                                <option value="<?=$uf['id'];?>" <?=$select?>><?=$uf['name'];?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<!-- page content end -->