<?php
//load dependencies
$this->template->javascript
                           ->add_first('assets/vendor/handlebars.min.js')
                           ->add('assets/js/pages/product_create.min.js');
?>

<!-- page content start -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="uk-grid uk-margin-top" data-uk-grid-margin>
            <div class="uk-width-large-9-10 uk-container-center">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-large-5-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    DETALHES
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <form action="#" id="product_form" method="post">
                                    <input type="hidden" name="id" id="product_id" value="">

                                    <div class="uk-form-row">
                                        <label for="barcode">Código de Produto</label>
                                        <input type="text" class="md-input" id="barcode" name="barcode" value="">
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="description">Descrição de Produto</label>
                                        <input type="text" class="md-input" id="description" name="description" value="" required>
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="category">Categoria de Produto</label>
                                        <select name="category" id="category" class="md-input" required>
                                            <option value=""></option>
                                            <?php if(!empty($category_list)):?>
                                                <?php foreach ($category_list as $category):?>
                                                    <option value="<?=$category['id'];?>"><?=$category['name'];?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="unit_type">Unidade de medida</label>
                                        <select name="unit_type" id="unit_type" class="md-input">
                                            <option value=""></option>
                                            <?php if(!empty($unit_list)):?>
                                                <?php foreach ($unit_list as $unit):?>
                                                    <option value="<?=$unit['id']?>"><?=$unit['name']?></option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="unit_value">Valor de Unidade</label>
                                        <input type="text" class="md-input" id="unit_value" name="unit_value" value="">
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="unit_value">Preço de venda</label>
                                        <input type="text" class="md-input money" id="price" name="price" value="">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="uk-width-large-5-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    PREÇO POR FORNECEDOR
                                </h3>
                            </div>
                            <div class="md-card-content" id="price_by_provider" data-dynamic-fields="provider_template" dynamic-fields-counter="0">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="md-fab-wrapper">
                    <button id="delete_product" class="md-fab md-fab-small md-fab-wave waves-effect waves-button" style="display: none">
                        <i class="material-icons">delete</i>
                    </button>
                    <button id="edit_product" class="md-fab md-fab-accent md-fab-wave waves-effect waves-button" style="display: none">
                        <i class="material-icons">edit</i>
                    </button>
                    <button id="save_product" class="md-fab md-fab-accent md-fab-wave waves-effect waves-button">
                        <i class="material-icons">save</i>
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- page content end -->


<script id="provider_template" type="text/x-handlebars-template">
    <div class="uk-grid form_section wrapper_dynamic_fields" data-uk-grid-margin data-id="{{index}}">
        <div class="uk-width-medium-2-3">
            <label for="provider{{counter}}">Fornecedor</label>
            <select name="provider{{counter}}" id="provider{{counter}}" class="md-input provider_select" required>
                <option value=""></option>
            </select>
        </div>
        <div class="uk-width-medium-1-3">
            <div class="uk-input-group">
                <label for="price{{counter}}">Preço</label>
                <input class="md-input money" type="text" id="price{{counter}}" name="price{{counter}}">
                <span class="uk-input-group-addon">
                    <a href="#" class="btnSectionClone">
                        <i class="material-icons md-24">&#xE146;</i>
                    </a>
                </span>
            </div>
        </div>
    </div>
</script>