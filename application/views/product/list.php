<?php
//load dependencies
$this->template->javascript
                           ->add_first('assets/vendor/handlebars.min.js')
                           ->add('assets/js/pages/product_list.min.js');
?>

<!-- page content start -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="md-card uk-width-large-7-10 uk-container-center">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label for="search_name">Product Name</label>
                        <input type="text" class="md-input" id="search_name">
                    </div>

                    <div class="uk-width-medium-3-10">
                        <div class="uk-margin-small-top">
                            <select id="category_list">
                                <option value="">Categoria</option>
                                <?php if(!empty($category_list)):?>
                                    <?php foreach ($category_list as $category):?>
                                        <option value="<?=$category['id'];?>" ><?=$category['name'];?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <div class="uk-margin-top uk-text-nowrap">
                            <input type="checkbox" id="filter_in_stock" name="filter_in_stock" value="true" data-md-icheck/>
                            <label for="filter_in_stock" class="inline-label">Em Estoque</label>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10 uk-text-center">
                        <button id="submit_search" class="md-btn md-btn-primary md-btn-block uk-margin-small-top">Buscar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-card uk-margin-medium-bottom uk-width-large-7-10 uk-container-center">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <div id="product_list">
                        <table class="uk-table uk-table-nowrap">
                            <?php if(!empty($products)): ?>
                                <thead>
                                    <th class="uk-width-3-10 ">Descrição</th>
                                    <th class="uk-width-2-10 ">Categoria</th>
                                    <th class="uk-width-1-10 ">Estoque</th>
                                    <th class="uk-width-1-10 small_col"></th>
                                </thead>
                                <tbody>
                                <?php foreach ($products as $product): ?>
                                    <tr data-id="<?= $product['id']; ?>" >
                                        <td class="product_description">
                                            <?= $product['description']; ?>
                                        </td>
                                        <td class="product_category" data-id="<?= $product['category']; ?>">
                                        </td>
                                        <td>
                                            <?= empty($product['qnt']) ? '0' : $product['qnt']; ?>
                                        </td>
                                        <td>
                                            <a class="view" href="<?= base_url("product/view/{$product['id']}"); ?>"><i class="md-icon material-icons">visibility</i></a>
                                            <a class="edit" href="<?= base_url("product/view/{$product['id']}/edit"); ?>"><i class="md-icon material-icons">create</i></a>
                                            <a class="delete" href="#"><i class="md-icon material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            <?php else:?>
                                <h3 class="uk-text-center uk-margin-top">Não há produtos cadastrados!</h3>
                            <?php endif; ?>
                        </table>

                        <?php
                        if(!empty($pagination))
                            echo $pagination;
                        ?>
                    </div>

                    <div id="result_list" style="display: none">
                        <table class="uk-table uk-table-nowrap">
                            <thead>
                                <th class="uk-width-3-10 ">Descrição</th>
                                <th class="uk-width-2-10 ">Categoria</th>
                                <th class="uk-width-1-10 ">Estoque</th>
                                <th class="uk-width-1-10 small_col"></th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div></div>
                    </div>
                    <div id="result_error" style="display: none">
                        <h3 class="uk-text-center uk-margin-top">Nenhum resultado foi encontrado!</h3>
                        <div class="uk-flex uk-flex-center">
                            <a href="#" class="uk-position-relative" id="back_search">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-fab-wrapper">
            <button class="md-fab md-fab-small md-fab-wave waves-effect waves-button" data-uk-modal="{target:'#modal_unit_type', bgclose:false}">
                <i class="material-icons">category</i>
            </button>
            <button class="md-fab md-fab-small md-fab-wave waves-effect waves-button" data-uk-modal="{target:'#modal_category', bgclose:false}">
                <i class="material-icons">local_offer</i>
            </button>
            <a class="md-fab md-fab-accent md-fab-wave waves-effect waves-button" href="<?= base_url('product/create'); ?>">
                <i class="material-icons">add</i>
            </a>
        </div>

    </div>
</div>
<!-- page content end -->


<!-- model category start -->
<div class="uk-modal" id="modal_category">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Categoria de Produto</h3>
        </div>

        <div data-dynamic-fields="field_template_category" id="wrapper_fields_category" dynamic-fields-counter="<?=end($category_list)['id']+1;?>">
            <?php if(!empty($category_list)): ?>
                <?php foreach ($category_list as $category):?>

                    <div class="uk-grid form_section" data-id="<?=$category['id'];?>">
                        <div class="uk-width-1-1">
                            <div class="uk-input-group">
                                <label for="category__<?=$category['id'];?>">Categoria</label>
                                <input type="text" class="md-input" id="category__<?=$category['id'];?>" name="category__<?=$category['id'];?>" data-id="<?=$category['id'];?>" value="<?=$category['name'];?>" disabled="disabled">
                                <span class="uk-input-group-addon">
                                    <a href="#" class="btnSectionRemove" disabled="disabled">
                                        <i class="material-icons md-24">&#xE872;</i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>

                <?php endforeach;?>
            <?php endif;?>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" id="close_category" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" id="save_category" class="md-btn md-btn-flat md-btn-flat-primary" style="display: none">Save</button>
            <button type="button" id="edit_category" class="md-btn md-btn-flat md-btn-flat-primary">Edit</button>
        </div>
    </div>
</div>
<!-- model category end -->

<!-- template field category start -->
<script id="field_template_category" type="text/x-handlebars-template">
    <div class="uk-grid form_section">
        <div class="uk-width-1-1">
            <div class="uk-input-group">
                <label for="category_{{counter}}">Categoria</label>
                <input type="text" class="md-input" name="category_{{counter}}" data-id="{{index}}" id="category_{{counter}}">
                <span class="uk-input-group-addon">
                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                </span>
            </div>
        </div>
    </div>
</script>
<!-- template field category end -->


<!-- model unit type start -->
<div class="uk-modal" id="modal_unit_type">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Unidades de Medida</h3>
        </div>

        <div data-dynamic-fields="field_template_unit_type" id="wrapper_fields_unit_type" dynamic-fields-counter="<?=end($unit_list)['id']+1;?>">
            <?php if(!empty($unit_list)): ?>
                <?php foreach ($unit_list as $unit_type):?>

                    <div class="uk-grid form_section" data-id="<?=$unit_type['id'];?>">
                        <div class="uk-width-1-1">
                            <div class="uk-input-group">
                                <label for="unit__<?=$unit_type['id'];?>">name</label>
                                <input type="text" class="md-input" id="unit__<?=$unit_type['id'];?>" name="unit__<?=$unit_type['id'];?>" data-id="<?=$unit_type['id'];?>" value="<?=$unit_type['name'];?>" disabled="disabled">
                                <span class="uk-input-group-addon">
                                    <a href="#" class="btnSectionRemove" disabled="disabled">
                                        <i class="material-icons md-24">&#xE872;</i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>

                <?php endforeach;?>
            <?php endif;?>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" id="close_unit" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" id="save_unit" class="md-btn md-btn-flat md-btn-flat-primary" style="display: none">Save</button>
            <button type="button" id="edit_unit" class="md-btn md-btn-flat md-btn-flat-primary">Edit</button>
        </div>
    </div>
</div>
<!-- model unit type end -->

<!-- template field unit type start -->
<script id="field_template_unit_type" type="text/x-handlebars-template">
    <div class="uk-grid form_section">
        <div class="uk-width-1-1">
            <div class="uk-input-group">
                <label for="unit_{{counter}}">Name</label>
                <input type="text" class="md-input" name="unit_{{counter}}" data-id="{{index}}" id="unit_{{counter}}">
                <span class="uk-input-group-addon">
                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                </span>
            </div>
        </div>
    </div>
</script>
<!-- template field unit type end -->