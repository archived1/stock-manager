<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function send_email($message, $subject, $sendTo, $fromEmail = null, $fromName = null)
    {
        $CI =& get_instance();
        $CI->load->config('email');

        $fromEmail = $fromEmail ? $fromEmail : $CI->config->item('smtp_user');
        $fromName  = $fromName ? $fromName : 'STR Tools';

        $CI->email->from($fromEmail, $fromName);
        $CI->email->to($sendTo);
        $CI->email->subject($subject);
        $CI->email->message($message);
        return $CI->email->send();
    }

// load templates for email!
function load_template_email($template, $data = array())
{
    $CI =& get_instance();

    if(file_exists(APPPATH."views/templates_email/{$template}.php")){
        $body = $CI->load->view("templates_email/includes/head", null, TRUE);
        $body .= $CI->load->view("templates_email/includes/hello", null, TRUE);
        $body .= $CI->load->view("templates_email/{$template}", $data, TRUE);
        $body .= $CI->load->view("templates_email/includes/thanks", null, TRUE);
        $body .= $CI->load->view("templates_email/includes/footer", null, TRUE);
        return $body;
    }

    return false;
}
