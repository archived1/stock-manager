<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// check if there is a section
function session_check()
{
    $CI =& get_instance();

    if(!$CI->session->has_userdata('logged_in'))
        redirect('');
}

// check if it's a recption by ajax
function ajax_check()
{
    $CI =& get_instance();

    if (!$CI->input->is_ajax_request())
        redirect(base_url());
}

// notify
function notify($mensagem, $status = 'info', $position = 'bottom-center') {
    $CI =& get_instance();
    $data['status']     = $status;
    $data['mensagem']   = $mensagem;
    $data['position']   = $position;
    $notify = $CI->session->flashdata('notify') ? $CI->session->flashdata('global_notify') : array();

    array_push($notify,$data);

    $CI->session->set_flashdata('global_notify', $notify);
}

// get events
function get_current_events(){
    $CI =& get_instance();
    $CI->load->model('Event_model');
    return $CI->Event_model->get_result(5,0, ['date_start' => date('Y-m-d'), 'date_end' => date('Y-m-d')]);
}