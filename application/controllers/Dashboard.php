<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        session_check();

        $this->load->model('Client_model');
        $this->load->model('Provider_model');
        $this->load->model('Product_model');
        $this->load->model('Order_model');

        $this->template->title->set('Dashboard');
        $this->template->set_template('templates/base');
    }

    public function index()
    {
        $this->load->model('Event_model');
        $data['events_list'] = $this->Event_model->get_result();

        $widget['client']   = $this->Client_model->count_results();
        $widget['provider'] = $this->Provider_model->count_results();
        $widget['product']  = $this->Product_model->count_results();
        $widget['orders'] = $this->Order_model->count_results(['type' => 2,'status' => 2]);
        $data['widget'] = $widget;

        $this->template->content->add('dashboard',$data);
        $this->template->render();
    }
}
