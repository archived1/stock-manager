<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Authentication extends CI_Controller
{
    public function __Construct()
    {
        parent::__Construct();

        $this->load->model('User_model');
    }

    public function index()
    {
        if($this->session->userdata('logged_in'))
            redirect(base_url("dashboard"));

        $this->template->set_template('templates/authentication');
        $this->template->render();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }


    /** Ajax Requisition **/

    public function login()
    {
        ajax_check();

        $data = $this->input->post();
        $response = $this->User_model->validate_login($data);

        if($response['status']) {
            $newdata = array(
                'user_id'   => $response['id'],
                'username'  => $response['username'],
                'role'      => $response['role'],
                'logged_in' => TRUE,
            );
            $this->session->set_userdata($newdata);

            $json = array('status' => 1, 'url' => base_url('dashboard'));
        }
        else
            $json = array('status' => 0, 'mensagem' => $response['error']);

        $this->template->render_json($json);
    }

    public function register()
    {
        ajax_check();
    }

    public function change_password()
    {
        ajax_check();

        $user_id = $this->session->userdata('user_id');
        $data = $this->input->post();
        $response = $this->User_model->change_password($data, $user_id);

        if($response['status'])
            $json = array('status' => 1, 'message' => $response['message']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }
}