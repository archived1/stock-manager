<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Service extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function city()
    {
        ajax_check();

        $data['uf'] = $this->input->post('id');

        $this->load->model('Address_model');
        $json = $this->Address_model->get_city($data);

        $this->template->render_json($json);
    }

    public function add_event()
    {
        ajax_check();

        $data['title']      = $this->input->post('title');
        $data['date']       = $this->input->post('date');
        $data['timeStart']  = $this->input->post('timeStart');
        $data['timeEnd']    = $this->input->post('timeEnd');
        $data['url']        = $this->input->post('url');

        $this->load->model('Event_model');
        $response = $this->Event_model->create($data);

        if($response['status'])
            $json = array('status' => 1, 'event_id' => $response['status']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function delete_event()
    {
        ajax_check();

        $event_id = $this->input->post('event_id');

        $this->load->model('Event_model');
        $response = $this->Event_model->remove($event_id);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function setting()
    {
        $data = $this->input->post();

        if(!empty($data['sidebar']))
            $this->session->set_userdata(['sidebar' => $data['sidebar']]);

    }

}