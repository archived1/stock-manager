<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Product extends CI_Controller
{
    private $num_result = 10;

    public function __Construct()
    {
        parent::__Construct();

        session_check();

        $this->load->model('Product_model');

        $this->template->title->set('Produtos');
        $this->template->set_template('templates/base');
    }

    public function index()
    {
        $row = $this->num_result;
        $total_rows = $this->Product_model->count_results();

        if($this->uri->segment(2))
            $offset = ($this->uri->segment(2) - 1) * $row;
        else
            $offset = 0;

        $data['products'] = $this->Product_model->get_result($row, $offset);
        $data['category_list'] = $this->Product_model->get_category();
        $data['unit_list'] = $this->Product_model->get_unit();
        $data['pagination'] = $this->pagination($row, $total_rows, 'product');
        $this->template->content->add('product/list', $data);
        $this->template->data->add_global('_category_list', $data['category_list']);
        $this->template->data->add_global('_unit_list', $data['unit_list']);
        $this->template->render();
    }

    public function create()
    {
        $data['category_list'] = $this->Product_model->get_category();
        $data['unit_list'] = $this->Product_model->get_unit();
        $this->template->content->add('product/create', $data);
        $this->template->data
                             ->add_global('product', array())
                             ->add_global('price_list', array())
                             ->add_global('provider_list', $this->provider_list())
                             ->add_global('select_provider', array());
        $this->template->render();
    }

    public function view($id)
    {
        if(empty($id) && !filter_var($id, FILTER_VALIDATE_INT))
            redirect(base_url('product'));

        $data['product'] = $this->Product_model->get($id);

        if(empty($data['product']))
            redirect(base_url('product'));

        $data['edit'] = $this->uri->segment(4) == 'edit' ? true : false;
        $data['category_list'] = $this->Product_model->get_category();
        $data['unit_list'] = $this->Product_model->get_unit();
        $data['price_list'] = $this->Product_model->get_price($id);

        $this->template->content->add('product/view', $data);
        $this->template->data
                             ->add_global('product', $data['product'])
                             ->add_global('price_list', $data['price_list'])
                             ->add_global('provider_list', $this->provider_list())
                             ->add_global('select_provider', array());
        $this->template->render();
    }


    /** Ajax Requisition **/

    public function search()
    {
        ajax_check();

        $search = $this->input->post('search');
        $filter = $this->input->post('filter');
        $cur_page = $this->input->post('cur_page');

        $row = $this->num_result;
        $total_rows = $this->Product_model->count_results(null, array('description' => $search));

        if(!empty($cur_page))
            $offset = ($cur_page - 1) * $row;
        else
            $offset = 0;

        $data['results'] = $this->Product_model->search($row, $offset, $filter, $search);
        $data['pagination'] = $this->pagination($row, $total_rows, null, 3, $cur_page);

        if(!empty($data['results']))
            $json = array('status' => 1, 'results' => $data['results'], 'pagination' => $data['pagination']);
        else
            $json = array('status' => 0);

        $this->template->render_json($json);
    }

    public function save()
    {
        ajax_check();

        $data['product_id'] = $this->input->post('product_id');
        $data['product'] = $this->input->post('product');
        $data['prices'] = $this->input->post('price_by_provider');

        if(empty($data['product_id']))
            $response = $this->_create($data);
        else
            $response = $this->_update($data);

        if($response['status'])
            $json = array('status' => 1, 'response' => $response['response']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function delete()
    {
        ajax_check();

        $product_id = $this->input->post('product_id');
        $response = $this->Product_model->remove($product_id);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function category()
    {
        ajax_check();

        $data = $this->input->post('data');
        $response['status'] = true;
        $response['response'] = array();
        $response['error'] = array();
        $res = array();


        if(!empty($data['add'])){
            foreach ($data['add'] as $category) {
                $r = $this->Product_model->create_category(array('name' => $category['name']));
                $r['response'] = ['id' => $r['status'], 'name' => $category['name'], 'target' => $category['id']];
                $res[] = $r;
            }
        }

        if(!empty($data['update'])){
            foreach ($data['update'] as $category)
                $res[] = $this->Product_model->update_category($category['id'], array('name' => $category['name']));
        }

        if(!empty($data['del'])){
            foreach ($data['del'] as $category)
                $res[] = $this->Product_model->remove_category($category['id'], 'id');
        }


        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response'][] = $r['response'];

            $response['error'][] = $r['error'];
        }


        if($response['status'])
            $json = array('status' => 1, 'response' => $response['response']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function unit()
    {
        ajax_check();

        $data = $this->input->post('data');
        $response['status'] = true;
        $response['response'] = array();
        $response['error'] = array();
        $res = array();


        if(!empty($data['add'])){
            foreach ($data['add'] as $unit) {
                $r = $this->Product_model->create_unit(array('name' => $unit['name']));
                $r['response'] = ['id' => $r['status'], 'name' => $unit['name'], 'target' => $unit['id']];
                $res[] = $r;
            }
        }

        if(!empty($data['update'])){
            foreach ($data['update'] as $unit)
                $res[] = $this->Product_model->update_unit($unit['id'], array('name' => $unit['name']));
        }

        if(!empty($data['del'])){
            foreach ($data['del'] as $unit)
                $res[] = $this->Product_model->remove_unit($unit['id'], 'id');
        }


        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response'][] = $r['response'];

            $response['error'][] = $r['error'];
        }


        if($response['status'])
            $json = array('status' => 1, 'response' => $response['response']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }


    /** Helpers **/

    private function pagination($row, $total_rows, $base_url, $uri_segment = 2, $cur_page = null)
    {
        $this->load->library('pagination');
        if(!empty($cur_page))
            $config['cur_page']     = $cur_page;
        $config['base_url']         = (!empty($base_url) ?  base_url($base_url) : false);
        $config['total_rows']       = $total_rows;
        $config['per_page']         = $row;
        $config['uri_segment']      = $uri_segment;
        $config['num_links']        = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="uk-pagination uk-margin-medium-top">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['first_tag_open']   = false;
        $config['first_tag_close']  = false;
        $config['last_link']        = false;
        $config['last_tag_open']    = false;
        $config['last_tag_close']   = false;
        $config['next_link']        = '<i class="uk-icon-angle-double-right"></i>';
        $config['next_tag_open']    = '<li><span>';
        $config['next_tag_close']   = '</span></li>';
        $config['prev_link']        = '<i class="uk-icon-angle-double-left"></i>';
        $config['prev_tag_open']    = '<li><span>';
        $config['prev_tag_close']   = '</span></li>';
        $config['cur_tag_open']     = '<li class="uk-active"><span>';
        $config['cur_tag_close']    = '</span></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        // $config['attributes']       = array('class' => 'page-link');
        // $config['attributes']['rel']= FALSE;
        $this->pagination->initialize($config);
        $return = $this->pagination->create_links();

        return $return;
    }

    private function provider_list()
    {
        $this->load->model('Provider_model');
        $provider_list = $this->Provider_model->get_result();
        $list = array();

        foreach ($provider_list as $provider)
            $list[] = ['value' => $provider['id'], 'text' => $provider['trade'], 'status' => true];

        return $list;
    }

    private function _create($data)
    {
        $response['status'] = true;
        $response['response'] = array();
        $response['error'] = array();
        $res = array();

        // create product
        $product = $this->Product_model->create($data['product']);

        if(!$product['status'])
            return $response;

        if(!empty($data['prices']['add'])){
            foreach ($data['prices']['add'] as $price) {
                $target = $price['id'];
                unset($price['id']);
                $price['product'] = $product['status'];
                $r = $this->Product_model->create_price($price);
                settype($r['status'], "string");
                $r['response'] = ['id' => $r['status'], 'provider' => $price['provider'], 'product' => $product['status'], 'price' => $price['price'], 'target' => $target];
                $res[] = $r;
            }
        }

        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response']['price_list'][] = $r['response'];

            $response['error'][] = $r['error'];
        }

        $response['response']['product'] = $product['status'];

        return $response;
    }

    private function _update($data)
    {
        $response['status'] = true;
        $response['response'] = null;
        $response['error'] = array();
        $res = array();


        if(!empty($data['product']))
            $res[] = $this->Product_model->update($data['product_id'], $data['product']);

        if(!empty($data['prices']['add'])){
            foreach ($data['prices']['add'] as $price) {
                $target = $price['id'];
                unset($price['id']);
                $r = $this->Product_model->create_price($price);
                settype($r['status'], "string");
                $r['response'] = ['id' => $r['status'], 'provider' => $price['provider'], 'product' => $data['product_id'], 'price' => $price['price'], 'target' => $target];
                $res[] = $r;
            }
        }

        if(!empty($data['prices']['update'])){
            foreach ($data['prices']['update'] as $price)
                $res[] = $this->Product_model->update_price($price['id'], $price['changes']);
        }

        if(!empty($data['prices']['del'])){
            foreach ($data['prices']['del'] as $price)
                $res[] = $this->Product_model->remove_price($price['id'], 'id');
        }


        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response']['price_list'][] = $r['response'];

            $response['error'][] = $r['error'];
        }

        return $response;
    }
}