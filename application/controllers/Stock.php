<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Stock extends CI_Controller
{
    private $num_result = 10;

    public function __Construct()
    {
        parent::__Construct();

        session_check();

        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->load->model('Provider_model');

        $this->template->title->set('Stock');
        $this->template->set_template('templates/base');
    }

    public function index()
    {
        $rows_stock_entry = $this->Order_model->count_results(array('type' => 1));
        $rows_stock_out = $this->Order_model->count_results(array('type' => 2));

        $data['stock_entry'] = $this->Order_model->get_result($this->num_result, 0,['type' => 1]);
        $data['stock_out'] = $this->Order_model->get_result($this->num_result, 0, ['type' => 2]);
        $data['status_list'] = [1 => 'pendente', 2 => 'finalizado'];
        $data['provider_list'] = $this->provider_list();
        $data['pagination']['entry'] = $this->pagination($this->num_result, $rows_stock_entry, null);
        $data['pagination']['out'] = $this->pagination($this->num_result, $rows_stock_out, null);
        $this->template->content->add('stock/list', $data);
        $this->template->data
                             ->add_global('filter', array())
                             ->add_global('order_type', 1)
                             ->add_global('status_list', $data['status_list'])
                             ->add_global('provider_list', $data['provider_list']);
        $this->template->render();
    }

    public function entry($id = null)
    {
        $data['order'] = ['id' => '', 'provider' => '', 'discount' => '0,00', 'discount_type' => '' , 'data' => '', 'obs' => '', 'status' => '', 'type' => 1];
        $data['item_list'] = array();
        $data['item_last'] = 0;
        $data['provider_list'] = $this->provider_list();
        $data['prices_by_provider'] = array();

        if(!empty($id) && filter_var($id, FILTER_VALIDATE_INT)) {
            $order = $this->Order_model->get($id, 1);
            if($order) {
                $data['order'] = $order;
                $data['item_list'] = $this->Order_model->get_item($order['id']);
                $data['item_last'] = end($data['item_list'])['id'] + 1;
                $data['prices_by_provider'][$order['provider']] = $this->Product_model->get_price($order['provider'], 'provider');
            }
        }

        $data['status'] = $data['order']['status'] == 2 ? 'finish' : (empty($id) ? 'editable' : ($this->uri->segment(4) == 'edit' ? 'editable' : 'preview'));
        $this->template->content->add('stock/entry', $data);
        $this->template->data->add_global('order', $data['order'])
                             ->add_global('item_list', $data['item_list'])
                             ->add_global('item_last', $data['item_last'])
                             ->add_global('prices_by_provider', $data['prices_by_provider'])
                             ->add_global('provider_selected', $data['order']['provider']);
        $this->template->render();

    }

    public function out($id = null)
    {
        $data['order'] = ['id' => '', 'provider' => '', 'discount' => '0,00', 'discount_type' => '' , 'data' => '', 'obs' => '', 'status' => '', 'type' => 2];
        $data['item_list'] = array();
        $data['item_last'] = 0;
        $data['provider_list'] = $this->client_list();

        if(!empty($id) && filter_var($id, FILTER_VALIDATE_INT)) {
            $order = $this->Order_model->get($id,2);
            if($order) {
                $data['order'] = $order;
                $data['item_list'] = $this->Order_model->get_item($order['id']);
                $data['item_last'] = end($data['item_list'])['id'] + 1;
            }
        }

        $data['status'] = $data['order']['status'] == 2 ? 'finish' : (empty($id) ? 'editable' : ($this->uri->segment(4) == 'edit' ? 'editable' : 'preview'));
        $this->template->content->add('stock/out', $data);
        $this->template->data->add_global('order', $data['order'])
                             ->add_global('item_list', $data['item_list'])
                             ->add_global('item_last', $data['item_last'])
                             ->add_global('product_list', array());
        $this->template->render();
    }


    /** Ajax Requisition **/

    public function search()
    {
        ajax_check();

        $filter = $this->input->post('filter');
        $type = $this->input->post('type');
        $cur_page = $this->input->post('cur_page');

        if(!empty($type))
            $filter['type'] = $type;

        $row = $this->num_result;
        $total_rows = $this->Order_model->count_results($filter);

        if(!empty($cur_page))
            $offset = ($cur_page - 1) * $row;
        else
            $offset = 0;

        $data['results'] = $this->Order_model->get_result($row, $offset, $filter);
        $data['pagination'] = $this->pagination($row, $total_rows, null, 2, $cur_page);

        if(!empty($data['results']))
            $json = array('status' => 1, 'results' => $data['results'], 'pagination' => $data['pagination'], 'filter' => $filter);
        else
            $json = array('status' => 0);

        $this->template->render_json($json);
    }

    public function search_product()
    {
        ajax_check();

        $search = $this->input->post('keyword');

        $data['results'] = $this->Product_model->search(10,0,null,$search);

        if(!empty($data['results']))
            $json = array('status' => 1, 'results' => $data['results']);
        else
            $json = array('status' => 0);

        $this->template->render_json($json);
    }

    public function get_prices()
    {
        ajax_check();

        $provider = $this->input->post('provider');
        $data = $this->Product_model->get_price($provider, 'provider');

        if(!empty($data))
            $json = array('status' => 1, 'results' => $data);
        else
            $json = array('status' => 0);

        $this->template->render_json($json);
    }

    public function save()
    {
        ajax_check();

        $data['order_id'] = $this->input->post('order_id');
        $data['order_status'] = $this->input->post('order_status');
        $data['order'] = $this->input->post('order');
        $data['itens'] = $this->input->post('itens');

        if(empty($data['order_id']))
            $response = $this->_create($data);
        else
            $response = $this->_update($data);

        if($response['status'])
            $json = array('status' => 1, 'response' => $response['response']);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }

    public function delete()
    {
        ajax_check();

        $order_id = $this->input->post('order_id');
        $response = $this->Order_model->remove($order_id);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'message' => $response['error']);

        $this->template->render_json($json);
    }


    /** Helpers **/

    private function pagination($row, $total_rows, $base_url, $uri_segment = 2, $cur_page = null)
    {
        $this->load->library('pagination');
        if(!empty($cur_page))
            $config['cur_page']     = $cur_page;
        $config['base_url']         = (!empty($base_url) ?  base_url($base_url) : false);
        $config['total_rows']       = $total_rows;
        $config['per_page']         = $row;
        $config['uri_segment']      = $uri_segment;
        $config['num_links']        = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="uk-pagination uk-margin-medium-top">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['first_tag_open']   = false;
        $config['first_tag_close']  = false;
        $config['last_link']        = false;
        $config['last_tag_open']    = false;
        $config['last_tag_close']   = false;
        $config['next_link']        = '<i class="uk-icon-angle-double-right"></i>';
        $config['next_tag_open']    = '<li><span>';
        $config['next_tag_close']   = '</span></li>';
        $config['prev_link']        = '<i class="uk-icon-angle-double-left"></i>';
        $config['prev_tag_open']    = '<li><span>';
        $config['prev_tag_close']   = '</span></li>';
        $config['cur_tag_open']     = '<li class="uk-active"><span>';
        $config['cur_tag_close']    = '</span></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        // $config['attributes']       = array('class' => 'page-link');
        // $config['attributes']['rel']= FALSE;
        $this->pagination->initialize($config);
        $return = $this->pagination->create_links();

        return $return;
    }

    private function provider_list()
    {
        $this->load->model('Provider_model');
        $provider_list = $this->Provider_model->get_result(null,null,['status' => 1]);
        $list = array();

        foreach ($provider_list as $provider)
            $list[$provider['id']] = ['id' => $provider['id'], 'trade' => $provider['trade']];

        return $list;
    }

    private function client_list()
    {
        $this->load->model('Client_model');
        $client_list = $this->Client_model->get_result(null,null,['status' => 1]);
        $list = array();

        foreach ($client_list as $client)
            $list[$client['id']] = ['id' => $client['id'], 'trade' => $client['trade']];

        return $list;
    }

    private function _create($data)
    {
        $response['status'] = true;
        $response['response'] = array();
        $response['error'] = array();
        $res = array();

        // create order
        $order = $this->Order_model->create($data['order']);
        $res[]= $order;

        if(!$order['status'])
            return $response;

        // create itens from order
        if(!empty($data['itens']['add'])) {
            foreach ($data['itens']['add'] as $item) {
                $target = $item['id'];
                unset($item['id']);
                $item['order'] = $order['status'];
                $r = $this->Order_model->create_item($item);
                settype($r['status'], "string");
                settype($order['status'], "string");
                $r['response'] = ['id' => $r['status'], 'order' => $order['status'], 'product' => $item['product'], 'price' => $item['price'], 'qnt' => $item['qnt'], 'target' => $target];
                $res[] = $r;
            }
        }

        // reorder response
        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response']['itens_list'][] = $r['response'];

            $response['error'][] = $r['error'];
        }

        $response['response']['order'] = $order['status'];

        return $response;
    }

    private function _update($data)
    {
        $response['status'] = true;
        $response['response'] = null;
        $response['error'] = array();
        $res = array();


        if(!empty($data['order']))
            $res[] = $this->Order_model->update($data['order_id'], $data['order']);

        // create itens from order
        if(!empty($data['itens']['add'])) {
            foreach ($data['itens']['add'] as $item) {
                $target = $item['id'];
                $item['order'] = $data['order_id'];
                unset($item['id']);

                $r = $this->Order_model->create_item($item);
                settype($r['status'], "string");
                $r['response'] = ['id' => $r['status'], 'order' => $data['order_id'], 'product' => $item['product'], 'price' => $item['price'], 'qnt' => $item['qnt'], 'target' => $target];
                $res[] = $r;
            }
        }

        // update itens from order
        if(!empty($data['itens']['update'])){
            foreach ($data['itens']['update'] as $item)
                $res[] =$this->Order_model->update_item($item['id'], $item['changes']);
        }

        // remove itens from order
        if(!empty($data['itens']['del'])){
            foreach ($data['itens']['del'] as $item)
                $res[] = $this->Order_model->remove_item($item['id']);
        }

        // finish
        if($data['order_status'] == 'finish')
            $this->Order_model->finish($data['order_id']);


        // reorder response
        foreach ($res as $r)
        {
            if(!$r['status'])
                $response['status'] = false;

            if(isset($r['response']))
                $response['response']['itens_list'][] = $r['response'];

            $response['error'][] = $r['error'];
        }

        return $response;
    }
}