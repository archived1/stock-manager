<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Client extends CI_Controller
{
    private $num_result = 10;

    public function __construct()
    {
        parent::__construct();

        session_check();

        $this->load->model('Client_model');
        $this->load->model('Address_model');

        $this->template->title->set('Client');
        $this->template->set_template('templates/base');
    }

    public function index()
    {
        $row = $this->num_result;
        $total_rows = $this->Client_model->count_results();

        if($this->uri->segment(2))
            $offset = ($this->uri->segment(2) - 1) * $row;
        else
            $offset = 0;

        $data['clients']  = $this->Client_model->get_result($row, $offset, ['status' => 1]);
        $data['pagination'] = $this->pagination($row, $total_rows, 'client');
        $this->template->content->add('client/list', $data);
        $this->template->render();
    }

    public function create()
    {
        $data['edit'] = true;
        $data['create_profile'] = true;
        $data['client'] = ['id' => '', 'trade' => '', 'owner' => '', 'company' => '', 'cnpj' => '', 'email' => '', 'phone' => '', 'street' => '', 'n' => '', 'cep' => '', 'district' => '', 'city' => '', 'uf' => ''];
        $data['uf_list'] = $this->Address_model->get_uf();
        $this->template->content->add('client/view', $data);
        $this->template->data
                             ->add_global('client_profile', array())
                             ->add_global('city_list', array());
        $this->template->render();
    }

    public function view($id)
    {
        $data['client'] = $this->Client_model->get($id);

        if(empty($data['client']))
            redirect(base_url('client'));

        $search['uf'] = $data['client']['uf'];
        $data['city_list'] = $this->Address_model->get_city($search);
        $data['uf_list']   = $this->Address_model->get_uf();
        $data['edit'] = $this->uri->segment(4) == 'edit' ? true : false;
        $data['create_profile'] = false;

        $this->template->content->add('client/view', $data);
        $this->template->data->add_global('client_profile', $data['client']);
        $this->template->data->add_global('city_list', array($data['city_list'][0]['uf'] => $data['city_list']));
        $this->template->render();
    }


    /** Ajax Requisition **/

    public function search()
    {
        $search =  $this->input->post('search');
        $cur_page = $this->input->post('cur_page');

        $row = $this->num_result;
        $total_rows = $this->Client_model->count_results(null, array('trade' => $search));

        if(!empty($cur_page))
            $offset = ($cur_page - 1) * $row;
        else
            $offset = 0;

        $data['results'] = $this->Client_model->search($search, $row, $offset);
        $data['pagination'] = $this->pagination($row, $total_rows, null, 3, $cur_page);

        if(!empty($data['results']))
            $json = array('status' => 1, 'results' => $data['results'], 'pagination' => $data['pagination']);
        else
            $json = array('status' => 0);

        $this->template->render_json($json);
    }

    public function save()
    {
        ajax_check();

        $data['trade']      = $this->input->post('trade');
        $data['company']    = $this->input->post('company');
        $data['cnpj']       = $this->input->post('cnpj');
        $data['owner']      = $this->input->post('owner');
        $data['phone']      = $this->input->post('phone');
        $data['email']      = $this->input->post('email');
        $data['street']     = $this->input->post('street');
        $data['n']          = $this->input->post('n');
        $data['cep']        = $this->input->post('cep');
        $data['district']   = $this->input->post('district');
        $data['city']       = $this->input->post('city');
        $data['uf']         = $this->input->post('uf');

        $response = $this->Client_model->create($data);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'mensagem' => $response['error']);

        $this->template->render_json($json);

    }

    public function edit()
    {
        ajax_check();

        $id = $this->input->post('id');
        $data = $this->input->post('data');

        $response = $this->Client_model->update($id,$data);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'mensagem' => $response['error']);

        $this->template->render_json($json);
    }

    public function delete()
    {
        ajax_check();

        $id = $this->input->post('id');
        $response = $this->Client_model->remove($id);

        if($response['status'])
            $json = array('status' => 1);
        else
            $json = array('status' => 0, 'mensagem' => $response['error']);

        $this->template->render_json($json);
    }


    /** Helpers **/

    private function pagination($row, $total_rows, $base_url, $uri_segment = 2, $cur_page = null)
    {
        $this->load->library('pagination');
        if(!empty($cur_page))
            $config['cur_page']     = $cur_page;
        $config['base_url']         = (!empty($base_url) ?  base_url($base_url) : false);
        $config['total_rows']       = $total_rows;
        $config['per_page']         = $row;
        $config['uri_segment']      = $uri_segment;
        $config['num_links']        = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="uk-pagination uk-margin-medium-top">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['first_tag_open']   = false;
        $config['first_tag_close']  = false;
        $config['last_link']        = false;
        $config['last_tag_open']    = false;
        $config['last_tag_close']   = false;
        $config['next_link']        = '<i class="uk-icon-angle-double-right"></i>';
        $config['next_tag_open']    = '<li><span>';
        $config['next_tag_close']   = '</span></li>';
        $config['prev_link']        = '<i class="uk-icon-angle-double-left"></i>';
        $config['prev_tag_open']    = '<li><span>';
        $config['prev_tag_close']   = '</span></li>';
        $config['cur_tag_open']     = '<li class="uk-active"><span>';
        $config['cur_tag_close']    = '</span></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        // $config['attributes']       = array('class' => 'page-link');
        // $config['attributes']['rel']= FALSE;
        $this->pagination->initialize($config);
        $return = $this->pagination->create_links();

        return $return;
    }
}