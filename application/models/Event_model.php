<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Event_model extends CI_Model
{
    private $table = 'event';

    public function get($id)
    {
        return $this->db->where('id',$id)
                        ->get($this->table)
                        ->row_array();
    }

    public function get_result($limit = null, $start = null, $filter = array())
    {
        if(!empty($filter['date_start']))
            $this->db->where('date >=', date('Y-m-d', strtotime($filter['date_start'])));

        if(!empty($filter['date_end']))
            $this->db->where('date <=', date('Y-m-d', strtotime($filter['date_end'])));

        $this->db->limit($limit, $start);
        $reponse = $this->db->get($this->table)->result_array();
        return $reponse;
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;
        $response['error'] = '';

        if($response['status']) {
            $response['error'] = '';
        }
        else
            $response['error'] = 'Ocorreu uma falha ao criar o evento';

        return $response;
    }

    public function remove($id)
    {
        $this->db->where('id', $id)->delete($this->table);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover o evento';

        return $response;
    }

    public function count_results($where = array(), $like = array())
    {
        $this->db->from($this->table);

        if(!empty($where['date_start']))
            $this->db->where('date >=', $where['date_start']);

        if(!empty($where['date_end']))
            $this->db->where('date <=', $where['date_end']);

        if(!empty($like))
            $this->db->like($like);

        return $this->db->count_all_results();
    }
}