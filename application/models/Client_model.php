<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Client_model extends CI_Model
{
    private $table = 'client';

    public function __construct()
    {
        parent::__construct();
    }

    public function search($value, $limit, $start, $field = 'trade')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('status',1);
        $this->db->like($field,$value);
        $this->db->limit($limit, $start);
        $reponse = $this->db->get()->result_array();

        return $reponse;
    }

    public function get($id)
    {
        return $this->db->from($this->table)
                        ->where('id',$id)->get()->row_array();
    }

    public function get_result($limit = '', $start = '', $where = array())
    {
        if(!empty($start))
            $this->db->limit($limit, $start);

        if(!empty($where))
            $this->db->where($where);

        $reponse = $this->db->get($this->table)->result_array();

        return $reponse;
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao criar o cliente!';

        return $response;
    }

    public function update($id, $data)
    {
        $this->db->where('id',$id)->update($this->table,$data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;
        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao editar o cliente!';

        return $response;
    }

    public function remove($id)
    {
        $this->db->where('id', $id)->update($this->table,['status' => 0]);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = 'Ocorreu uma falha ao remover o cliente!';
        else
            $response['error'] = '';

        return $response;
    }

    public function count_results($where = array(), $like = array())
    {
        $this->db->from($this->table);
        $this->db->where('status',1);
        if($where)
            $this->db->where($where);
        if($like)
            $this->db->like($like);
        return $this->db->count_all_results();
    }
}