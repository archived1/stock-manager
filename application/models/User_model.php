<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_model extends CI_Model
{
    private $table = 'user';

    public function __construct()
    {
        parent::__construct();
    }

    public function create($data)
    {
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $this->db->insert($this->table,$data);
        return $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;
    }

    public function update($data,$id)
    {
        if(!empty($data['password']))
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

        $this->db->where('id', $id);
        $this->db->update($this->table,$data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function remove($id)
    {

    }

    public function validate_login($data)
    {
        $this->db->where('username', $data['username']);
        $response = $this->db->get($this->table)->row_array();
        $response['status'] = false;
        $response['error']  = '';

        if(empty($response['id'])) {
            $response['error'] = 'Usuário não cadastrado!';
            return $response;
        }

        if(!password_verify($data['password'], $response['password'])) {
            $response['error'] = 'Senha inválida!';
            unset($response['password']);
            return $response;
        }

        unset($response['password']);
        $response['status'] = true;
        return $response;
    }

    public function change_password($data,$user_id)
    {
        $user = $this->db->get_where($this->table, array('id' => $user_id))->row_array();
        $response['status'] = false;
        $response['message'] = '';
        $response['error']  = '';

        if(empty($user['id'])) {
            $response['error'] = 'Usuário não encontrado!';
            return $response;
        }

        if(!password_verify($data['currentPassword'], $user['password'])) {
            $response['error'] = 'Senha inválida!';
            return $response;
        }

        $data = array(
            'password' => password_hash($data['newPassword'], PASSWORD_DEFAULT),
        );

        $this->db->where('id',$user_id);
        $this->db->update('user', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['message'] = 'Senha atualizada com sucesso!';

        return $response;
    }
}