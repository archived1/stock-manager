<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Product_model extends CI_Model
{
    private $table = 'product';

    public function __construct()
    {
        parent::__construct();
    }

    public function search($limit, $start, $filter, $value, $field = 'description')
    {
        $this->db->select('*');
        $this->db->from($this->table);

        if(!empty($filter['stock']))
            $this->db->where('qnt >=',1);

        if(!empty($filter['category']))
            $this->db->where('category', $filter['category']);

        if(!empty($value))
            $this->db->like($field,$value);

        $this->db->limit($limit, $start);
        $reponse = $this->db->get()->result_array();
        return $reponse;
    }

    public function get($id)
    {
        return $this->db->where('id',$id)
                        ->get($this->table)->row_array();
    }

    public function get_result($limit = null, $start = null)
    {
        $reponse = $this->db->limit($limit, $start)
                            ->get($this->table)
                            ->result_array();
        return $reponse;
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status']) {
            $response['error'] = '';
        }
        else
            $response['error'] = 'Ocorreu uma falha ao criar o produto';

        return $response;
    }

    public function update($id, $data)
    {
        $this->db->where('id',$id)->update($this->table,$data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao editar o produto';

        return $response;
    }

    public function remove($id)
    {
        $this->remove_price($id);

        $this->db->where('id', $id)->delete($this->table);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = 'Ocorreu uma falha ao remover o produto!';
        else
            $response['error'] = '';

        return $response;
    }

    public function count_results($where = array(), $like = array())
    {
        $this->db->from($this->table);

        if($where)
            $this->db->where($where);
        if($like)
            $this->db->like($like);

        return $this->db->count_all_results();
    }


    // product category

    public function get_category($id = null)
    {
        $this->db->from('product_category');

        if(!empty($id))
            $this->db->where('id', $id);

        return $this->db->get()->result_array();
    }

    public function create_category($data)
    {
        $this->db->insert('product_category', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao salvar a categoria de produto!';

        return $response;
    }

    public function update_category($id, $data)
    {
        $this->db->where('id',$id)->update('product_category',$data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao editar a categoria de produto!';

        return $response;
    }

    public function remove_category($id)
    {
        $this->db->where('id', $id)->delete('product_category');
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover a categoria de produto!';

        return $response;
    }


    // unit type

    public function get_unit($id = null)
    {
        $this->db->from('unit_type');

        if(!empty($id))
            $this->db->where('id', $id);

        return $this->db->get()->result_array();
    }

    public function create_unit($data)
    {
        $this->db->insert('unit_type', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao salvar a unidade de medida!';

        return $response;
    }

    public function update_unit($id, $data)
    {
        $this->db->where('id',$id)->update('unit_type',$data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao editar a unidade de medida!';

        return $response;
    }

    public function remove_unit($id)
    {
        $this->db->where('id', $id)->delete('unit_type');
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover a unidade de medida!';

        return $response;
    }


    // price by provider

    public function get_price($value, $type = 'product')
    {
        $this->db->from('price_by_provider');

        if(!empty($value))
            $this->db->where($type, $value);

        return $this->db->get()->result_array();
    }

    public function create_price($data)
    {
        $this->db->insert('price_by_provider', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = "Ocorreu uma falha ao salvar o preço do produto por fornecedor!";

        return $response;
    }

    public function update_price($id,$data)
    {
        $this->db->where('id',$id)->update('price_by_provider',$data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao editar o preço por fornecedor!';

        return $response;
    }

    public function remove_price($value, $type = 'product')
    {
        $this->db->where($type, $value)->delete('price_by_provider');
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover o preço por fornecedor!';

        return $response;
    }
}