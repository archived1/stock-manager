<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Address_model extends CI_Model
{
    private $table_city = 'city';
    private $table_uf   = 'uf';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_city($data = null, $limit = null, $start = null, $search = null)
    {
        $this->db->from($this->table_city);

        if(!empty($data))
            $this->db->where($data);

        if(!empty($search))
            $this->db->like($this->table_city, $search);

        if(!empty($limit) && !empty($start))
            $this->db->limit($limit, $start);

        $response = $this->db->get()->result_array();

        return !empty($response) ? $response : false;
    }

    public function get_uf($data = null)
    {
        $this->db->from($this->table_uf);

        if(!empty($data))
            $this->db->where($data);

        $response = $this->db->get();

        if(!empty($data))
            $response = $response->row_array();
        else
         $response = $response->result_array();

        return !empty($response) ? $response : false;
    }
}