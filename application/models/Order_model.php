<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Order_model extends CI_Model
{
    private $table = 'order';

    public function get($id, $type = null)
    {
        $type = $type == 1 ? 'provider' : ($type == 2 ? 'client' : null);
        $select = empty($type) ? '' : 'p.trade, p.company';

        $this->db->select("o.*, {$select}");
        $this->db->from('order o');

        if(!empty($type))
            $this->db->join("{$type} p", 'p.id = o.provider','left');

        $this->db->where('o.id',$id);
        return $this->db->get()->row_array();
    }

    public function get_result($limit = null, $start = null, $filter = array())
    {
        $type = !empty($filter['type']) ? ($filter['type'] == 1 ? 'provider' : ($filter['type'] == 2 ? 'client' : null) ) : null;
        $select = empty($type) ? '' : 'p.trade, p.company';

        $this->db->select("o.*, {$select}");
        $this->db->from('order o');

        if(!empty($type))
            $this->db->join("{$type} p", 'p.id = o.provider','left');

        if(!empty($filter['date_start']))
            $this->db->where('date >=', date('Y-m-d', strtotime($filter['date_start'])));

        if(!empty($filter['date_end']))
            $this->db->where('date <=', date('Y-m-d', strtotime($filter['date_end'])));

        if(!empty($filter['status']))
            $this->db->where('status', $filter['status']);

        if(!empty($filter['type']))
            $this->db->where('type', $filter['type']);

        $this->db->limit($limit, $start);
        $reponse = $this->db->get()->result_array();
        return $reponse;
    }

    public function create($data)
    {
        $data['date'] = date('Y-m-d H:i:s');
        $data['status'] = 1;
        $this->db->insert($this->table, $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;
        $response['error'] = '';

        if($response['status']) {
            $response['error'] = '';
        }
        else
            $response['error'] = 'Ocorreu uma falha ao registrar movimentações de estoque';

        return $response;
    }

    public function update($id, $data)
    {
        $this->db->where('id',$id)->update($this->table, $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao atualizar movimentações de estoque';

        return $response;
    }

    public function remove($id)
    {
        $order = $this->get($id);
        if($order['status'] == 2) {
            $response = $this->reverse($id);

            if(!$response['status'])
                return $response;
        }

        $this->remove_item($id, 'order');
        $this->db->where('id', $id)->delete($this->table);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover o pedido';

        return $response;
    }

    public function count_results($where = array(), $like = array())
    {
        $this->db->from($this->table);

        if(!empty($where['date_start']))
            $this->db->where('date >=', $where['date_start']);

        if(!empty($where['date_end']))
            $this->db->where('date <=', $where['date_end']);

        if(!empty($where['status']))
            $this->db->where('status', $where['status']);

        if(!empty($where['type']))
            $this->db->where('type', $where['type']);

        if(!empty($like))
            $this->db->like($like);

        return $this->db->count_all_results();
    }


    // itens from order stock

    public function get_item($value, $type = 'order')
    {
        $this->db->select('i.*, p.qnt as stock_qnt');
        $this->db->from('item_order i');
        $this->db->join('product p', 'p.id = i.product','left');
        $this->db->where($type, $value);
        return $this->db->get()->result_array();
    }

    public function create_item($data)
    {
        $this->db->insert('item_order', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? $this->db->insert_id() : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = "Ocorreu uma falha ao registrar movimentações de estoque";

        return $response;
    }

    public function update_item($id, $data)
    {
        $this->db->where('id', $id)->update('item_order', $data);
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao atualizar item de estoque';

        return $response;
    }

    public function remove_item($value, $type = 'id')
    {
        $this->db->where($type, $value)->delete('item_order');
        $response['status'] = $this->db->affected_rows() >= 1 ? true : false;

        if($response['status'])
            $response['error'] = '';
        else
            $response['error'] = 'Ocorreu uma falha ao remover item';

        return $response;
    }


    // add qnt product
    public function add_qnt_product($id,$qnt)
    {
        $this->db->where('id', $id);
        $this->db->set('qnt', "qnt+{$qnt}", FALSE);
        $this->db->update('product');
    }

    // remove qnt product
    public function remove_qnt_product($id,$qnt)
    {   try {
            $product = $this->db->where('id', $id)->get('product')->first_row();

            if(!($product->qnt >= $qnt))
                throw new \Exception('Erro: quantidade de estoque é inferior à quantidade definida para estorno!');

            $this->db->where('id', $id);
            $this->db->set('qnt', "qnt-{$qnt}", FALSE);
            $this->db->update('product');
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
            return $response;
        }
    }

    public function finish($order_id)
    {
        $response['status'] = false;
        $response['error'] = '';


        $order = $this->get($order_id);
        if(empty($order)) {
            $response['error'] = 'Erro: Pedido não foi encontrado';
            return $response;
        }
        if($order['status'] == 2) {
            $response['error'] = 'Erro: Pedido já foi finalizado';
            return $response;
        }

        $itens = $this->get_item($order_id);
        if(empty($itens)) {
            $response['error'] = 'Erro: Os itens do pedido não foram encontrados';
            return $response;
        }


        if($order['type'] == 1) {
            foreach ($itens as $item)
                $this->add_qnt_product($item['product'], $item['qnt']);
        }

        if($order['type'] == 2) {
            foreach ($itens as $item)
                $this->remove_qnt_product($item['product'], $item['qnt']);
        }


        $finish = $this->update($order['id'], array('status' => 2));
        $response['status'] = $finish['status'];

        return $response;
    }

    public function reverse($order_id)
    {
        $response['status'] = false;
        $response['error'] = '';


        $order = $this->get($order_id);
        if(empty($order)) {
            $response['error'] = 'Erro: Pedido não foi encontrado!';
            return $response;
        }
        if($order['status'] == 1) {
            $response['error'] = 'Erro: Pedido não foi finalizado';
            return $response;
        }

        $itens = $this->get_item($order_id);
        if(empty($itens)) {
            $response['error'] = 'Erro: Os itens do pedido não foram encontrados';
            return $response;
        }


        if($order['type'] == 1) {
            foreach ($itens as $item)
                $this->remove_qnt_product($item['product'], $item['qnt']);
        }

        if($order['type'] == 2) {
            foreach ($itens as $item)
                $this->add_qnt_product($item['product'], $item['qnt']);
        }


        $reverse = $this->update($order['id'], array('status' => 1));
        $response['status'] = $reverse['status'];

        return $response;
    }
}