<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Template configuration
| -------------------------------------------------------------------
| This file will contain the settings for the template library.
|
| 'parser' = if you want your views file to be parsed, set to TRUE
| 'template' = template path
 */

$config['parser']   = false;
$config['compress'] = true;
$config['template'] = null;