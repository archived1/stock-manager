<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_event extends CI_Migration
{
    protected $table = 'event';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'title' => [
                'type' => 'VARCHAR(64)',
            ],
            'date' => [
                'type' => 'DATE',
            ],
            'timeStart' => [
                'type' => 'time',
            ],
            'timeEnd' => [
                'type' => 'time',
            ],
            'url' => [
                'type' => 'VARCHAR(255)',
            ],
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}