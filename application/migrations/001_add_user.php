<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_user extends CI_Migration
{
    protected $table = 'user';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'username' => [
                'type' => 'VARCHAR(32)',
            ],
            'password' => [
                'type' => 'VARCHAR(64)',
            ],
            'role' => [
                'type' => 'INT(1)',
            ],
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);

        $this->db->insert($this->table, [
            'username' => "admin",
            'password' => password_hash('admin', PASSWORD_DEFAULT),
            'role'     => 1,
        ]);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}