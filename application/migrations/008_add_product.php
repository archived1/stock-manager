<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_product extends CI_Migration
{
    protected $table = 'product';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'description' => [
                'type' => 'VARCHAR(62)'
            ],
            'barcode' => [
                'type' => 'VARCHAR(42)'
            ],
            'qnt' => [
                'type' => 'VARCHAR(10)',
            ],
            'category' => [
                'type' => 'VARCHAR(10)',
            ],
            'unit_type' => [
                'type' => 'INT(10)',
            ],
            'unit_value' => [
                'type' => 'VARCHAR(22)'
            ],
            'price' => [
                'type' => 'DECIMAL(10,2)',
                'default' => 0.00
            ]
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}