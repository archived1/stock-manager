<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_order extends CI_Migration
{
    protected $table = 'order';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'provider' => [
                'type' => 'INT(10)',
            ],
            'date' => [
                'type' => 'DATETIME',
            ],
            'obs' => [
                'type' => 'VARCHAR(140)',
            ],
            'status' => [
                'type' => 'INT(1)',
                'default' => 1
            ],
            'type' => [
                'type' => 'INT(1)',
            ],
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}