<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_unit_type extends CI_Migration
{
    protected $table = 'unit_type';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'name' => [
                'type' => 'VARCHAR(42)'
            ]
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}