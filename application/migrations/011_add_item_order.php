<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_item_order extends CI_Migration
{
    protected $table = 'item_order';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'order' => [
                'type' => 'INT(10)',
            ],
            'product' => [
                'type' => 'INT(10)',
            ],
            'price' => [
                'type' => 'DECIMAL(10,2)',
                'default' => 0.00
            ],
            'qnt' => [
                'type' => 'INT(10)',
            ]
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}