<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_client extends CI_Migration
{
    protected $table = 'client';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(10)',
                'auto_increment' => TRUE,
            ],
            'trade' => [
                'type' => 'VARCHAR(42)'
            ],
            'company' => [
                'type' => 'VARCHAR(86)'
            ],
            'cnpj' => [
                'type' => 'VARCHAR(14)'
            ],
            'owner' => [
                'type' => 'VARCHAR(42)'
            ],
            'phone' => [
                'type' => 'VARCHAR(12)'
            ],
            'email' => [
                'type' => 'VARCHAR(62)'
            ],
            'street' => [
                'type' => 'VARCHAR(52)',
            ],
            'n' => [
                'type' => 'VARCHAR(6)',
            ],
            'cep' => [
                'type' => 'INT(8)',
            ],
            'district' => [
                'type' => 'VARCHAR(42)',
            ],
            'city' => [
                'type' => 'INT(4)'
            ],
            'uf' => [
                'type' => 'INT(2)'
            ],
            'status' => [
                'type' => 'INT(1)'
            ]
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);
    }
}