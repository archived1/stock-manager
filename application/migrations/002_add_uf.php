<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Migration_add_uf extends CI_Migration
{
    protected $table = 'uf';

    public function up()
    {
        $fields = array(
            'id' => [
                'type' => 'INT(2)',
                'auto_increment' => TRUE,
            ],
            'name' => [
                'type' => 'VARCHAR(32)',
            ]
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table, TRUE);

        $list_uf = array(
            'AC',
            'AL',
            'AP',
            'AM',
            'BA',
            'CE',
            'DF',
            'ES',
            'GO',
            'MA',
            'MT',
            'MS',
            'MG',
            'PA',
            'PB',
            'PE',
            'PI',
            'PR',
            'RJ',
            'RN',
            'RS',
            'RO',
            'RR',
            'SC',
            'SP',
            'SE',
            'TO'
        );

        foreach ($list_uf as $uf)
            $this->db->insert($this->table, ['name' => $uf]);
    }

    public function down()
    {
        if($this->db->table_exists($this->table))
            $this->dbforge->drop_table($this->table);

    }
}